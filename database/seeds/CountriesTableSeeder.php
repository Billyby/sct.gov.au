<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Afghanistan',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Albania',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Algeria',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'American Samoa',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Andorra',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Angola',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Anguilla',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Antarctica',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Antigua and Barbuda',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Argentina',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Armenia',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Aruba',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Ascension',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Australia',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Austria',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Azerbaijan',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Bahamas',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bahrain',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Bangladesh',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Barbados',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Belarus',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Belgium',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Belize',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Benin, Republic of',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Bermuda',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Bhutan',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Bolivia',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Bosnia and Herzegovina',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Botswana',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Brazil',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'British Virgin Islands',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Brunei',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Bulgaria',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Burkina Faso',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Burundi',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Cambodia',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Cameroon',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Canada',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Cape Verde Islands',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Cayman Islands',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Central African Rep',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Chad Republic',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Chatham Island, NZ',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Chile',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'China',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Christmas Island',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Cocos Islands',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Colombia',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Comoros',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Congo',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Cook Islands',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Costa Rica',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Croatia',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Cuba',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Curacao',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Cyprus',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Czech Republic',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Denmark',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Diego Garcia',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Djibouti',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Dominica',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Dominican Republic',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Easter Island',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Ecuador',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Egypt',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'El Salvador',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Equitorial Guinea',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Eritrea',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Estonia',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Ethiopia',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Falkland Islands',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Faroe Islands',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Fiji Islands',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Finland',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'France',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'French Antilles',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'French Guiana',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'French Polynesia',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Gabon Republic',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Gambia',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Georgia',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Germany',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Ghana',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Gibraltar',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Greece',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Greenland',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Grenada and Carriacuou',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Grenadin Islands',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Guadeloupe',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Guam',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Guantanamo Bay',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Guatemala',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Guiana',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Guinea, Bissau',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Guinea, Rep',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Guyana',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Haiti',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Honduras',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Hong Kong',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Hungary',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Iceland',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'India',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Indonesia',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Inmarsat',
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Iran',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Iraq',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Ireland',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Isle of Man',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Israel',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Italy',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Ivory Coast',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Jamaica',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Japan',
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Jordan',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Kazakhstan',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Kenya',
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Kiribati',
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Korea, North',
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Korea, South',
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Kosovo',
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Kuwait',
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Kyrgyzstan',
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Laos',
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Latvia',
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Lebanon',
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Lesotho',
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Liberia',
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Libya',
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Liechtenstein',
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Lithuania',
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Luxembourg',
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Macau',
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Macedonia, FYROM',
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Madagascar',
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Malawi',
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Malaysia',
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Maldives',
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Mali Republic',
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Malta',
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Mariana Islands',
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Marshall Islands',
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Martinique',
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Mauritania',
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Mauritius',
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Mayotte Island',
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Mexico',
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Micronesia, Fed States',
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Midway Islands',
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Miquelon',
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Moldova',
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Monaco',
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Mongolia',
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Montserrat',
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Morocco',
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Mozambique',
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'Myanmar',
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'Namibia',
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Nauru',
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Nepal',
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Neth. Antilles',
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Netherlands',
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Nevis',
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'New Caledonia',
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'New Zealand',
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Nicaragua',
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Niger Republic',
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Nigeria',
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Niue',
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Norfolk Island',
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Norway',
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'Oman',
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Pakistan',
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Palau',
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Panama',
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Papua New Guinea',
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Paraguay',
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Peru',
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Philippines',
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Poland',
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Portugal',
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Principe',
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Puerto Rico',
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Qatar',
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Reunion Island',
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Romania',
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Russia',
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'Rwanda',
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'Saipan',
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'San Marino',
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Sao Tome',
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Saudi Arabia',
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'Senegal Republic',
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Serbia, Republic of',
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Seychelles',
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'Sierra Leone',
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'Singapore',
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Slovakia',
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'Slovenia',
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'Solomon Islands',
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'Somalia Republic',
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'South Africa',
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'Spain',
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Sri Lanka',
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'St. Helena',
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'St. Kitts',
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'St. Lucia',
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'St. Pierre et Miquelon',
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'St. Vincent',
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Sudan',
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Suriname',
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Swaziland',
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Sweden',
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Switzerland',
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Syria',
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Taiwan',
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Tajikistan',
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Tanzania',
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Thailand',
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Togo',
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Tokelau',
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Tonga',
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Trinidad and Tobago',
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Tunisia',
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Turkey',
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Turkmenistan',
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'Turks and Caicos Islands',
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Tuvalu',
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'US Virgin Islands',
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Uganda',
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'Ukraine',
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'United Arab Emirates',
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'United Kingdom',
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'United States',
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'Uruguay',
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Uzbekistan',
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'Vanuatu',
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'Vatican city',
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Venezuela',
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Vietnam, Soc Republic of',
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'Wake Island',
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Wallis and Futuna Islands',
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Western Samoa',
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'Yemen',
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'Yugoslavia',
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'Zaire',
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'Zambia',
            ),
            246 => 
            array (
                'id' => 247,
                'name' => 'Zanzibar',
            ),
            247 => 
            array (
                'id' => 248,
                'name' => 'Zimbabwe',
            ),
            248 => 
            array (
                'id' => 249,
                'name' => 'Not Provided',
            ),
        ));
        
        
    }
}