<?php

use Illuminate\Database\Seeder;

class ComplaintTypesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('complaint_types')->delete();
        
        \DB::table('complaint_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Death Complaints'
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Disability Complaints'
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'All Other Complaints'
            )
        ));
        
    }
}