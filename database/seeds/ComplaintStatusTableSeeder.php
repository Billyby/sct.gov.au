<?php

use Illuminate\Database\Seeder;

class ComplaintStatusTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('complaint_status')->delete();
        
        \DB::table('complaint_status')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Draft'
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Submitted'
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Reviewed'
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Actioned'
            )
        ));
        
    }
}