<?php

use Illuminate\Database\Seeder;

class DisabilityTypesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('disability_types')->delete();
        
        \DB::table('disability_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Total and Permanent Disablement (TPD)'
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Salary Continuance (SC)'
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Total and Temporary Disablement (TTD)'
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Income Protection (IP)'
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Terminal Illness(TI)'
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Not Provided'
            )
        ));
        
    }
}