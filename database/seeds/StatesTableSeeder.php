<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('states')->delete();
        
        \DB::table('states')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Australian Capital Territory',
                'short_name' => 'ACT',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'New South Wales',
                'short_name' => 'NSW',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Northern Territory',
                'short_name' => 'NT',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Queensland',
                'short_name' => 'QLD',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'South Australia',
                'short_name' => 'SA',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Tasmania',
                'short_name' => 'TAS',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Victoria',
                'short_name' => 'VIC',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Western Australia',
                'short_name' => 'WA',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'International',
                'short_name' => 'INT',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Not Provided',
                'short_name' => '',
            ),
        ));
        
        
    }
}