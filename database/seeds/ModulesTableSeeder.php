<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Pages',
            'display_name' => 'Pages',
            'slug' => 'pages',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'News',
            'display_name' => 'News',
            'slug' => 'news',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Gallery',
            'display_name' => 'Gallery',
            'slug' => 'gallery',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Contact',
            'display_name' => 'Contact',
            'slug' => 'contact',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'FAQS',
            'display_name' => 'FAQS',
            'slug' => 'faqs',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Determinations',
            'display_name' => 'Determinations',
            'slug' => 'determinations',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
