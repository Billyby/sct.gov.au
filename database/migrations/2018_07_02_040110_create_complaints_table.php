<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->tinyInteger('complaint_made')->nullable();
            $table->tinyInteger('complaint_response')->nullable();
            $table->tinyInteger('over_90_days')->nullable();
            $table->tinyInteger('complaint_type_id')->nullable();
            $table->tinyInteger('complaint_status_id')->nullable();
            $table->string('title')->nullable();
            $table->string('surname')->nullable();
            $table->string('given_name')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->integer('dob')->nullable();
            $table->string('address')->nullable();
            $table->integer('state_id')->default(10);
            $table->string('postcode')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('fax')->nullable();
            $table->string('on_behalf_name_dob')->nullable();
            $table->string('relationship')->nullable();
            $table->tinyInteger('legal_guardian')->nullable();
            $table->tinyInteger('poa')->nullable();
            $table->string('super_provider_name')->nullable();
            $table->string('super_member_name')->nullable();
            $table->string('super_member_policy_number')->nullable();
            $table->string('rep_title')->nullable();
            $table->string('rep_surname')->nullable();
            $table->string('rep_given_name')->nullable();
            $table->string('rep_address')->nullable();
            $table->tinyInteger('rep_state_id')->default(10);
            $table->string('rep_postcode')->nullable();
            $table->integer('rep_country_id')->nullable();
            $table->string('rep_phone')->nullable();
            $table->string('rep_mobile')->nullable();
            $table->string('rep_email')->nullable();
            $table->string('rep_fax')->nullable();
            $table->string('rep_relationship')->nullable();
            $table->text('rep_reasons')->nullable();
            $table->text('complaint_details')->nullable();
            $table->text('complaint_unfair_details')->nullable();
            $table->decimal('complaint_amount_dispute', 10, 2)->nullable();
            $table->text('complaint_outcomes')->nullable();
            $table->string('complainant_name')->nullable();
            $table->integer('complaint_submitted_date')->default(0);
            $table->integer('complaint_created_date')->default(0);
            $table->integer('complaint_reviewed_date')->default(0);
            $table->integer('complaint_actioned_date')->default(0);
            $table->tinyInteger('actioned')->default(0);
            $table->string('complaint_receipt_number')->nullable();
            $table->text('notes')->nullable();
            $table->dateTime('modified')->nullable();
            $table->integer('cms_user_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
