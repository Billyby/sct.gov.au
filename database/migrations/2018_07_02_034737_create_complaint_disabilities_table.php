<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintDisabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaint_disabilities', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('complaint_id');
            $table->string('dis_type_ids')->nullable();
            $table->integer('dis_last_attended_date')->nullable();
            $table->integer('dis_ceased_work_date')->nullable();
            $table->text('dis_ceased_reasons')->nullable();
            $table->integer('dis_physical_mental')->nullable();
            $table->integer('dis_employer_notice')->nullable();
            $table->integer('dis_claim_lodge_date')->nullable();
            $table->integer('dis_worker_compensation')->nullable();
            $table->integer('dis_payment_start_date')->nullable();
            $table->integer('dis_payment_ceased_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaint_disabilities');
    }
}
