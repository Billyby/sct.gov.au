<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeterminationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('determination_categories', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->enum('status', ['active','passive'])->default('passive');
            $table->integer('position')->default(1);
            $table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });


        Schema::create('determinations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('category_id');
            $table->string('determination_number')->nullable();
            $table->string('file_number')->nullable();
            $table->text('description')->nullable();
            $table->string('austlii_file')->nullable();
            $table->text('summary')->nullable();
            $table->string('determination_file')->nullable();
            $table->integer('determination_date')->nullable();
            $table->enum('status', ['active','passive'])->default('passive');
            $table->integer('cms_user_id')->nullable();
            $table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });

        Schema::create('determination_dates', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->string('determination_number')->nullable();
            $table->string('date')->nullable();
            $table->integer('date_unix')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
