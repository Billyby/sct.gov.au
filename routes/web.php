<?php 
// Home Page
Route::get('/', 'HomeController@index');

// Contact Form
Route::group(['prefix' => '/contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::get('success', 'ContactController@success');
});

// Pages Module
Route::group(['prefix' => '/pages'], function () {
    Route::get('{category}', 'PagesController@index');    
	Route::get('{category}/{page}', 'PagesController@index');    
});

// Newsletters Route
Route::group(['prefix' => '/newsletters'], function () {
	Route::get('/', function() {
	   return(Redirect::to('/news/newsletters'));
	});
	
	Route::get('/{page}', function($page) {
	   return(Redirect::to('/news/newsletters/' . $page));
	});
});

// Latest News Route
Route::group(['prefix' => '/latest-news'], function () {
	Route::get('/', function() {
	   return(Redirect::to('/news/latest-news'));
	});
	
	Route::get('/{page}', function($page) {
	   return(Redirect::to('/news/latest-news/' . $page));
	});
});

// News Module
Route::group(['prefix' => '/news'], function () {
	Route::get('', 'NewsController@list'); 	
	
	Route::get('archive', 'NewsController@archive'); 
	Route::get('archive/{age}', 'NewsController@archive'); 
	
    Route::get('{category}', 'NewsController@list');    
	Route::get('{category}/{page}', 'NewsController@item'); 
	
});

// Faqs Module
Route::group(['prefix' => '/faqs'], function () {
	Route::get('', 'FaqsController@index'); 
    Route::get('{category}', 'FaqsController@index');    	
});

// Members Module
Route::group(['prefix' => '/members'], function () {
	Route::get('', 'MembersController@index'); 
    Route::get('{category}', 'MembersController@index');    	
});

// Download Route
Route::group(['prefix' => '/downloads'], function () {
	Route::get('/', function() {
	   return(Redirect::to('/documents'));
	});
	
	Route::get('/{page}', function($page) {
	   return(Redirect::to('/documents/' . $page));
	});
});

// Documents Module
Route::group(['prefix' => '/documents'], function () {
	Route::get('', 'DocumentsController@index'); 
    Route::get('{category}', 'DocumentsController@index');    	
});

// Gallery Module
Route::group(['prefix' => '/gallery'], function () {
	Route::get('', 'GalleryController@index'); 
    Route::get('{category}', 'GalleryController@detail');    	
});

// Projects Module
Route::group(['prefix' => '/projects'], function () {
	Route::get('', 'ProjectsController@list'); 			

    Route::get('{category}', 'ProjectsController@list');    
	Route::get('{category}/{page}', 'ProjectsController@item'); 
	
});

// Online Complaint
Route::group(['prefix' => '/online-complaint'], function () {
    /*Route::get('/', 'OnlineComplaintController@index');
    Route::post('personal-information-collection-notice', 'OnlineComplaintController@showPersonalInformationCollectionNotice');
    Route::any('registration-form', 'OnlineComplaintController@showRegistrationForm');
    Route::post('file-upload', 'OnlineComplaintController@saveComplaint');
    Route::any('upload', 'OnlineComplaintController@upload');
    Route::any('{id}/download', 'OnlineComplaintController@download');
    Route::post('{id}/delete', 'OnlineComplaintController@delete');
    Route::post('success', 'OnlineComplaintController@success');*/
    Route::get('{complaint_receipt_number}/pdf', 'OnlineComplaintController@pdf');
    Route::get('{complaint_receipt_number}/testpdf', 'OnlineComplaintController@testPdf');
});

// Search Determinations Module
Route::group(['prefix' => '/search-determinations'], function () {
    Route::get('', 'DeterminationsController@index');
    Route::any('results', 'DeterminationsController@results');
    Route::any('{id}/detail', 'DeterminationsController@detail');
    Route::any('{id}/download', 'DeterminationsController@download');
});

// Site Search
Route::group(['prefix' => '/results'], function () {
    Route::any('', 'SiteSearchController@index');
});

Route::get('/subscribe-thank-you', 'HomeController@subThankYou');
Route::get('/subscribe-confirm', 'HomeController@subConfirm');