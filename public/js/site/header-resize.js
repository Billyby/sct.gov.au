// JavaScript Document
function resizeHeaderOnScroll() {
   const distanceY = window.pageYOffset || document.documentElement.scrollTop,
   shrinkOn = 100;

   headerLogo = document.getElementById('header-logo-img');	   	   
   txtSearch = document.getElementById('txtSearch');	
 
   if (distanceY > shrinkOn) {      	  
	   $('#header-logo-img').removeClass('header-logo-img-larger').addClass('header-logo-img-smaller');
	   $('#txtSearch').removeClass('search-txt-larger').addClass('search-txt-smaller');
   } 
	
   if (distanceY == 0)  {
	  $('#header-logo-img').removeClass('header-logo-img-smaller').addClass('header-logo-img-larger');
	  $('#txtSearch').removeClass('search-txt-smaller').addClass('search-txt-larger');
   }
}

window.addEventListener('scroll', resizeHeaderOnScroll);

function IsSafariBrowser() {
  return /^((?!chrome).)*safari/i.test(navigator.userAgent);
}
