$(function () {
    'use strict';

    $('#fileupload').fileupload({
        datatype : 'json',
        url : 'upload',
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|doc|docx|xls|xlsx)$/i,
        maxNumberOfFiles: 10
    });

    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
        var inputs = data.context.find(':input');
        if (inputs.filter(function () {
            return !this.value && $(this).prop('required');
        }).first().focus().length) {
            data.context.find('button').prop('disabled', false);
            return false;
        }

        data.formData = inputs.serializeArray();
        data.formData['2'] = {'name':'id', 'value': $("input[name='id']").val()};
        data.formData['3'] = {'name':'receipt_number', 'value': $("input[name='receipt_number']").val()};

    });

    $('#fileupload').bind('fileuploaddone', function (e, data) {
        if($("#sub_cont").length>0 && $("#sub_cont").val()==1){
            $("#fileupload").submit();
        }
    });

    $("#submit-btn").click(function() {
        if($(".c-btn").length>0){
            $("#sub_cont").val('1');
            $(".start").click();
        }else{
            $("#fileupload").submit();
        }
    });


});