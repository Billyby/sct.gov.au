$(document).ready(function() {
    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_square-grey'
    });

    $('input[name="fc_officer"]').on('ifChecked', function(event){

        if($(this).val()=='Yes'){
            $('#funds-response').removeClass('invisible');
            $('input[name="fc_response"]').iCheck('uncheck');
            $('#fc-officer-note').addClass('invisible');
            $('#complaint-type').addClass('invisible');
            $('#submit').addClass('invisible');
        }

        if($(this).val()=='No'){
            $('#funds-response').addClass('invisible');
            $('#fc-officer-note').removeClass('invisible');
            $('#complaint-type').removeClass('invisible');
            $('input[name="c_type"]').iCheck('uncheck');
            $('#funds-response-no').addClass('invisible');
            $('#submit').addClass('invisible');
        }
    });

    $('input[name="fc_response"]').on('ifChecked', function(event){

        if($(this).val()=='Yes'){
            $('#complaint-type').removeClass('invisible');
            $('input[name="c_type"]').iCheck('uncheck');
            $('#funds-response-no').addClass('invisible');
        }

        if($(this).val()=='No'){
            $('#funds-response-no').removeClass('invisible');
            $('input[name="expiry"]').iCheck('uncheck');
            $('#complaint-type').addClass('invisible');
            $('#submit').addClass('invisible');
        }
    });

    $('input[name="expiry"]').on('ifChecked', function(event){

        if($(this).val()=='Yes'){
            $('#funds-response-no-note').addClass('invisible');
            $('#complaint-type').removeClass('invisible');
            $('input[name="c_type"]').iCheck('uncheck');
            $('#submit').addClass('invisible');
        }

        if($(this).val()=='No'){
            $('#complaint-type').removeClass('invisible');
            $('input[name="c_type"]').iCheck('uncheck');
            $('#funds-response-no-note').removeClass('invisible');
            $('#submit').addClass('invisible');
        }
    });

    $('input[name="c_type"]').on('ifChecked', function(event){
        $('#submit').removeClass('invisible');
    });
});