$(document).ready(function() {

    var form = $("#complaint-form");

    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); }
    });

    var settings = {
        headerTag: "h5",
        bodyTag: "section",
        stepsOrientation: "vertical",
        autoFocus: true,
        labels:
            {
                finish: "Submit and Go to File Upload",
            },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    };

    if(error_cont){
        settings.enableAllSteps = true;
        settings.startIndex = 5;
    }

    form.steps(settings);

    if(error_cont){
        $("li[role='tab']").removeClass('current').addClass('done');
        form.validate().settings.ignore = ":disabled";
    }

    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_square-grey'
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    });

});