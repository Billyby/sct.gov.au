<?php

return [
    'entities' => [
        'documents' => '\App\Document',
        'document-categories' => '\App\DocumentCategory',
        'faqs' => '\App\Faq',
        'faq-categories' => '\App\FaqCategory',
        'gallery-categories' => '\App\GalleryCategory',
        'members' => '\App\Member',
        'member-types' => '\App\MemberType',
        'news-categories' => '\App\NewsCategory',
    	'pages' => '\App\Page',
    	'page-categories' => '\App\PageCategory',
    	'projects' => '\App\Project',
    	'project-categories' => '\App\ProjectCategory',
    	'determination-categories' => '\App\DeterminationCategory',
        // 'articles' => '\Article' for simple sorting (entityName => entityModel) or
        // 'posts' => ['entity' => '\Post', 'relation' => 'tags'] for many to many or many to many polymorphic relation sorting
    ],
];
