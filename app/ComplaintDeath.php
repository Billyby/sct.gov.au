<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintDeath extends Model
{	
    protected $table = 'complaint_deaths';

    public function getDateDeceasedAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }
}
