<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMessages extends Model
{
    protected $table = 'contact_messages';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('message-filter');
        $select = "";

        if($filter['search']){
            $select =  $query->where('data','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
