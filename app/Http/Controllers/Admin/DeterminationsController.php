<?php

namespace App\Http\Controllers\Admin;

use App\Determination;
use App\DeterminationCategory;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Util\MimeType;

class DeterminationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $determinations = Determination::Filter()->where('is_deleted','=','false')->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
        } else {
            $determinations = Determination::where('is_deleted','=','false')->with('category')->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('determinations-filter');
        $categories = DeterminationCategory::orderBy('created_at', 'desc')->get();
        return view('admin/determinations/determinations', array(
            'determinations' => $determinations,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = DeterminationCategory::orderBy('created_at', 'desc')->get();
        return view('admin/determinations/add', array(
            'categories' => $categories
        ));
    }

    public function edit($determination_id)
    {
        $determination = Determination::where('id', '=', $determination_id)->first();
        $categories = DeterminationCategory::orderBy('created_at', 'desc')->get();
        return view('admin/determinations/edit', array(
            'determination' => $determination,
            'categories' => $categories
        ));
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
			'determination_number' => 'required',
			'file_number' => 'required',
			'description' => 'required',
			'determination_file' => 'required',
			'determination_date' => 'required',
        );

        $messages = [
            'category_id.required' => 'Please select category',
			'determination_number.required' => 'Please enter determination number',
			'file_number.required' => 'Please enter file number',
			'description.required' => 'Please enter description',
			'determination_file.required' => 'Please upload determination file',
			'determination_date.required' => 'Please enter determination date',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/determinations/add')->withErrors($validator)->withInput();
        }

        $file = $request->file('determination_file');
        $filename = $file->getClientOriginalName();
        $extension_pos = strrpos($filename, '.');
        $new_filename = substr($filename, 0, $extension_pos) . '_'.time() . substr($filename, $extension_pos);

        $determination = new Determination();
        $determination->category_id = $request->category_id;
        $determination->determination_number = $request->determination_number;
        $determination->file_number = $request->file_number;
        $determination->description = $request->description;
        $determination->summary = $request->summary;
        $determination->determination_file = $new_filename;
        $determination->determination_date = Carbon::createFromFormat('d/m/Y',$request->determination_date)->timestamp;
        $determination->determination_year = Carbon::createFromFormat('d/m/Y',$request->determination_date)->year;

        if($request->live=='on'){
            $determination->status = 'active';
        }

        $determination->cms_user_id = Auth::user()->id;
        $determination->save();

        Storage::putFileAs('determinations', $file, $new_filename);
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/determinations/' . $determination->id . '/edit')->with('message', Array('text' => 'Determination has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/determinations/')->with('message', Array('text' => 'Determination has been added', 'status' => 'success'));
		}

    }

    public function update(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'determination_number' => 'required',
            'file_number' => 'required',
            'description' => 'required',
            'determination_date' => 'required',
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'determination_number.required' => 'Please enter determination number',
            'file_number.required' => 'Please enter file number',
            'description.required' => 'Please enter description',
            'determination_date.required' => 'Please enter determination date',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/determinations/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $determination = Determination::where('id','=',$request->id)->first();
        $determination->category_id = $request->category_id;
        $determination->determination_number = $request->determination_number;
        $determination->file_number = $request->file_number;
        $determination->description = $request->description;
        $determination->summary = $request->summary;

        $determination->determination_date = Carbon::createFromFormat('d/m/Y',$request->determination_date)->timestamp;
        $determination->determination_year = Carbon::createFromFormat('d/m/Y',$request->determination_date)->year;

		if($request->live=='on'){
            $determination->status = 'active';
        } else {
            $determination->status = 'passive';
		}

		if($request->has('determination_file')){

            Storage::delete('determinations/'.$determination->determination_file);

            $file = $request->file('determination_file');
            $filename = $file->getClientOriginalName();
            $extension_pos = strrpos($filename, '.');
            $new_filename = substr($filename, 0, $extension_pos) . '_'.time() . substr($filename, $extension_pos);

            $determination->determination_file = $new_filename;

            Storage::putFileAs('determinations', $file, $new_filename);
		}

        $determination->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/determinations/' . $determination->id . '/edit')->with('message', Array('text' => 'Determination has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/determinations/')->with('message', Array('text' => 'Determination has been updated', 'status' => 'success'));
		}	
    }

    public function delete($determination_id)
    {
        $determination = Determination::where('id','=',$determination_id)->first();
        $determination->is_deleted = true;
        $determination->save();

        return \Redirect::back()->with('message', Array('text' => 'Determination has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $determination_id)
    {
        $determination = Determination::where('id', '=', $determination_id)->first();
        if ($request->status == "true") {
            $determination->status = 'active';
        } else if ($request->status == "false") {
            $determination->status = 'passive';
        }
        $determination->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = DeterminationCategory::orderBy('position', 'desc')->get();
        return view('admin/determinations/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/determinations/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:determination_categories'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/determinations/add-category')->withErrors($validator)->withInput();
        }

        $category = new DeterminationCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();
 
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/determinations/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/determinations/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}	

    }

    public function editCategory($category_id)
    {
        $category = DeterminationCategory::where('id', '=', $category_id)->first();
        return view('admin/determinations/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:determination_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/determinations/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = DeterminationCategory::findOrFail($request->id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/determinations/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/determinations/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = DeterminationCategory::where('id','=',$category_id)->first();

        if(count($category->determinations)){
            return \Redirect::to('dreamcms/determinations/categories')->with('message', Array('text' => 'Category has determinations. Please delete determinations first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/determinations/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = DeterminationCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';
        } else if ($request->status == "false") {
            $category->status = 'passive';
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = DeterminationCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/determinations/sort-category', array(
            'categories' => $categories
        ));
    }

    public function download($determination_id)
    {
        $determination = Determination::where('id','=',$determination_id)->first();
        $file = Storage::get('determinations/'.$determination->determination_file);
        $type = MimeType::detectByContent($file);

        $headers = [
            'Content-Type' => $type
        ];

        return response($file,200,$headers);
    }

    public function emptyFilter()
    {
        session()->forget('determinations-filter');
        return redirect()->to('dreamcms/determinations');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('determinations-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('determinations-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}