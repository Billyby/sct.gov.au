<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Document;
use App\DocumentCategory;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class DocumentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $documents = Document::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $documents = Document::with('category')->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('documents-filter');
        $categories = DocumentCategory::orderBy('created_at', 'desc')->get();
        return view('admin/documents/documents', array(
            'documents' => $documents,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = DocumentCategory::orderBy('created_at', 'desc')->get();
        return view('admin/documents/add', array(
            'categories' => $categories
        ));
    }

    public function edit($document_id)
    {
        $document = Document::where('id', '=', $document_id)->first();
        $categories = DocumentCategory::orderBy('created_at', 'desc')->get();
        return view('admin/documents/edit', array(
            'document' => $document,
            'categories' => $categories
        ));
    }

	public function preview($document_id)
    {
		$document = Document::with("category")->where('id', '=', $document_id)->first();		
		
		$general = new General();
		$view = $general->documentPreview($document->category->slug, $document->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
			'fileName' => 'required',
			'dateDocument' => 'required',
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'title.required' => 'Please enter title',
            'description.required' => 'Please enter description',
			'fileName.required' => 'Please upload a document',
			'dateDocument.required' => 'Please enter document date',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/documents/add')->withErrors($validator)->withInput();
        }

        $document = new Document();
        $document->category_id = $request->category_id;
        $document->title = $request->title;
		$document->slug = str_slug($request->title, '-');
        $document->description = $request->description;
		$document->fileName = $request->fileName;
		$document->dateDocument = date('Y-m-d' , strtotime(str_replace("/", "-", $request->dateDocument)));
        if($request->live=='on'){
           $document->status = 'active'; 
        }

        $document->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/documents/' . $document->id . '/edit')->with('message', Array('text' => 'Document has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/documents/')->with('message', Array('text' => 'Document has been added', 'status' => 'success'));
		}		        

    }

    public function update(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
			'fileName' => 'required',
			'dateDocument' => 'required',
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'title.required' => 'Please enter title',
            'description.required' => 'Please enter desciption',
			'fileName.required' => 'Please upload a document',
			'dateDocument.required' => 'Please enter document date',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/documents/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $document = Document::where('id','=',$request->id)->first();
        $document->category_id = $request->category_id;
        $document->title = $request->title;
		$document->slug = str_slug($request->title, '-');
        $document->description = $request->description;
		$document->fileName = $request->fileName;
		$document->dateDocument = date('Y-m-d' , strtotime(str_replace("/", "-", $request->dateDocument)));
		if($request->live=='on'){
           $document->status = 'active'; 
		} else {
			$document->status = 'passive';
        }
        $document->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/documents/' . $document->id . '/edit')->with('message', Array('text' => 'Document has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/documents/')->with('message', Array('text' => 'Document has been updated', 'status' => 'success'));
		}		       
    }

    public function delete($document_id)
    {
        $document = Document::where('id','=',$document_id)->first();
        $document->is_deleted = true;
        $document->save();

        return \Redirect::back()->with('message', Array('text' => 'Document has been deleted.', 'status' => 'success'));
    }

    public function changeDocumentStatus(Request $request, $document_id)
    {
        $document = Document::where('id', '=', $document_id)->first();
        if ($request->status == "true") {
            $document->status = 'active';
        } else if ($request->status == "false") {
            $document->status = 'passive';
        }
        $document->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $documents = Document::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/documents/sort', array(
            'documents' => $documents
        ));
    }

    public function categories()
    {
        $categories = DocumentCategory::orderBy('position', 'desc')->get();
        return view('admin/documents/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/documents/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:document_categories'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/documents/add-category')->withErrors($validator)->withInput();
        }

        $document = new DocumentCategory();
        $document->name = $request->name;		
		$document->slug = $request->slug;
        $document->description = $request->description;
		if($request->live=='on'){
           $document->status = 'active'; 
        }
        $document->save();
        
        if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/documents/' . $document->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/documents/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}
    }

    public function editCategory($category_id)
    {
        $category = DocumentCategory::where('id', '=', $category_id)->first();
        return view('admin/documents/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:document_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/documents/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = DocumentCategory::findOrFail($request->id);
        $category->name = $request->name;
		$category->slug = $request->slug;
        $category->description = $request->description;
		if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/documents/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/documents/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}       
    }

    public function deleteCategory($category_id)
    {
        $category = DocumentCategory::where('id','=',$category_id)->first();

        if(count($category->documents)){
            return \Redirect::to('dreamcms/documents/categories')->with('message', Array('text' => 'Category has documents. Please delete documents first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/documents/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = DocumentCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';
        } else if ($request->status == "false") {
            $category->status = 'passive';
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = DocumentCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/documents/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('documents-filter');
        return redirect()->to('dreamcms/documents');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('documents-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('documents-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }
}