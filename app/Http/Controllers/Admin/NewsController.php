<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use App\NewsCategory;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $news = News::Filter()->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
        } else {
            $news = News::with('category')->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('news-filter');
        $categories = NewsCategory::orderBy('created_at', 'desc')->get();
        return view('admin/news/news', array(
            'news' => $news,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = NewsCategory::orderBy('created_at', 'desc')->get();
        return view('admin/news/add', array(
            'categories' => $categories
        ));
    }

    public function edit($news_id)
    {
        $news = News::where('id', '=', $news_id)->first();
        $categories = NewsCategory::orderBy('created_at', 'desc')->get();
        return view('admin/news/edit', array(
            'news' => $news,
            'categories' => $categories
        ));
    }

	public function preview($news_id)
    {
		$news = News::with("category")->where('id', '=', $news_id)->first();		
		
		$general = new General();
		$view = $general->newsPreview($news->category->slug, $news->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
			'slug' => 'required|unique_store:news',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'short_description' => 'required',
            'start_date' => 'required',
            'archive_date' => 'required|date_checker:'.$request->start_date,
            'body' => 'required',
        );

        $messages = [
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_store' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'short_description.required' => 'Please enter short desciption',
            'start_date.required' => 'Please enter start date',
            'archive_date.required' => 'Please enter archive date',
            'archive_date.date_checker' => 'Archive date should be greater than the start date',
            'body.required' => 'Please enter body'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/news/add')->withErrors($validator)->withInput();
        }
       
        $news = new News();
        $news->category_id = $request->category_id;
        $news->title = $request->title;
		$news->slug = $request->slug;        
        $news->meta_title = $request->meta_title;
        $news->meta_keywords = $request->meta_keywords;
        $news->meta_description = $request->meta_description;
        $news->short_description = $request->short_description;
        $news->start_date = date('Y-m-d' , strtotime(str_replace("/", "-", $request->start_date))); 
        $news->archive_date = date('Y-m-d' , strtotime(str_replace("/", "-", $request->archive_date))); 
        $news->body = $request->body;
        $news->thumbnail = $request->thumbnail;

        if($request->live=='on'){
           $news->status = 'active'; 
        }

        $news->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/news/' . $news->id . '/edit')->with('message', Array('text' => 'News has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/news/')->with('message', Array('text' => 'News has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(
            'title' => 'required',
			'slug' => 'required|unique_update:news,' . $request->id,
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'short_description' => 'required',
            'start_date' => 'required',
            'archive_date' => 'required',
            'body' => 'required',
        );

        $messages = [
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_update' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'short_description.required' => 'Please enter short desciption',
            'start_date.required' => 'Please enter start date',
            'archive_date.required' => 'Please enter archive date',
            'archive_date.date_checker' => 'Archive date should be greater than the start date',
            'body.required' => 'Please enter body'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/news/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        //$start_date_arr = explode("/","$request->start_date");
        //$start_date = mktime(12,00,00,$start_date_arr[0],$start_date_arr[1],$start_date_arr[2]);

        //$archive_date_arr = explode("/",$request->archive_date);
        //$archive_date = mktime(12,00,00,$archive_date_arr[0],$archive_date_arr[1],$archive_date_arr[2]);

        $news = News::where('id','=',$request->id)->first();
        $news->category_id = $request->category_id;
        $news->title = $request->title;
        $news->slug = $request->slug;
        $news->meta_title = $request->meta_title;
        $news->meta_keywords = $request->meta_keywords;
        $news->meta_description = $request->meta_description;
        $news->short_description = $request->short_description;
        $news->start_date = date('Y-m-d' , strtotime(str_replace("/", "-", $request->start_date)));
        $news->archive_date = date('Y-m-d' , strtotime(str_replace("/", "-", $request->archive_date)));
        $news->body = $request->body;
        $news->thumbnail = $request->thumbnail;
		if($request->live=='on'){
           $news->status = 'active'; 
        } else {
			$news->status = 'passive'; 
		}
        $news->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/news/' . $news->id . '/edit')->with('message', Array('text' => 'News has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/news/')->with('message', Array('text' => 'News has been updated', 'status' => 'success'));
		}	
    }

    public function delete($news_id)
    {
        $news = News::where('id','=',$news_id)->first();
        $news->is_deleted = true;
        $news->save();

        return \Redirect::back()->with('message', Array('text' => 'News has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $news_id)
    {
        $news = News::where('id', '=', $news_id)->first();
        if ($request->status == "true") {
            $news->status = 'active';
        } else if ($request->status == "false") {
            $news->status = 'passive';
        }
        $news->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = NewsCategory::orderBy('position', 'desc')->get();
        return view('admin/news/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/news/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:news_categories'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/news/add-category')->withErrors($validator)->withInput();
        }

        $category = new NewsCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();
 
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/news/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/news/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}	

    }

    public function editCategory($category_id)
    {
        $category = NewsCategory::where('id', '=', $category_id)->first();
        return view('admin/news/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:news_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/news/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = NewsCategory::findOrFail($request->id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/news/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/news/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = NewsCategory::where('id','=',$category_id)->first();

        if(count($category->news)){
            return \Redirect::to('dreamcms/news/categories')->with('message', Array('text' => 'Category has news. Please delete news first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/news/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $faq_category_id)
    {
        $faq_category = NewsCategory::where('id', '=', $faq_category_id)->first();
        if ($request->status == "true") {
            $faq_category->status = 'active';
        } else if ($request->status == "false") {
            $faq_category->status = 'passive';
        }
        $faq_category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = NewsCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/news/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('news-filter');
        return redirect()->to('dreamcms/news');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('news-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('news-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}