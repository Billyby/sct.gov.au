<?php

namespace App\Http\Controllers\Admin;

use App\Complaint;
use App\ComplaintDocument;
use App\ComplaintType;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Util\MimeType;

class ComplaintsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $complaints = Complaint::Filter()->sortable()->where('is_finished','=','true')->orderBy('created_at', 'desc')->paginate($paginate_count);
        } else {
            $complaints = Complaint::sortable()->where('is_finished','=','true')->orderBy('created_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('complaints-filter');
        $types = ComplaintType::all();
        return view('admin/complaints/complaints', array(
            'complaints' => $complaints,
            'types' => $types,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function view($complaint_id)
    {
        $complaint = Complaint::where('id', '=', $complaint_id)->first();

        return view('admin/complaints/view', array(
            'complaint' => $complaint
        ));
    }

    public function download($document_id)
    {
        $document = ComplaintDocument::where('id','=',$document_id)->first();
        $file = Storage::get('complaint/'.$document->complaint_id.'/'.$document->complaint_document);
        $type = MimeType::detectByContent($file);

        $headers = [
            'Content-Type' => $type
        ];

        return response($file,200,$headers);
    }

    public function update(Request $request)
    {

        $complaint = Complaint::where('id', '=', $request->id)->first();

        $reviewed_date = 0;
        if($request->last_reviewed_date) {
            $reviewed_date_arr = explode('/', $request->last_reviewed_date);
            $reviewed_date = mktime(0, 0, 0, $reviewed_date_arr[1], $reviewed_date_arr[0], $reviewed_date_arr[2]);
        }

        $complaint->complaint_reviewed_date = $reviewed_date;

        if($reviewed_date!=0 && $complaint->complaint_status_id!=4){
            $complaint->complaint_status_id = 3;
        }

        $complaint->notes = $request->notes;

		if($request->actioned=='on'){
            $complaint->actioned = 1;
            $complaint->complaint_status_id = 4;

            if($complaint->complaint_actioned_date==0){
                $complaint->complaint_actioned_date = time();
            }

		} else {
            $complaint->actioned = 0;
            $complaint->complaint_actioned_date = 0;
        }

        $complaint->save();

        if ($request->action == 'save') {
            return \Redirect::to('dreamcms/complaints/' . $complaint->id . '/view')->with('message', Array('text' => 'Complaint has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/complaints')->with('message', Array('text' => 'Complaint has been updated', 'status' => 'success'));
        }
    }

    public function emptyFilter()
    {
        session()->forget('complaints-filter');
        return redirect()->to('dreamcms/complaints');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->type && $request->type != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('complaints-filter', [
                'type' => $request->type,
                'search' => $request->search
            ]);
        }

        if (session()->has('complaints-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }
}