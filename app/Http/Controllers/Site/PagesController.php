<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Page;
use App\PageCategory;

use App\Http\Controllers\Site\NewsController;
use App\Http\Controllers\Site\DocumentsController;
use App\Http\Controllers\Site\MembersController;

class PagesController extends Controller
{
    public function index($category_slug, $page_slug = "", $mode = ""){
		// News
		$news = new NewsController();
		
		// Documents
		$documents = new DocumentsController();
		$side_nav_documents = $documents->getCategories();
		
		// Pages
    	$category = $this->getCategory($category_slug);
		$side_nav = ($category[0]->slug != "news-and-publications" ? $this->getPages($category[0]->id) : $news->getCategories() );
		$page = ($page_slug == "" ? $this->getPages($category[0]->id)->first() : $this->getPage($category[0]->id, $page_slug, $mode));
		
		// Members
		if ($category[0]->slug == 'about-us' && $page->slug == 'who-we-are') { 
		    $member = new MembersController();
		    $members = $member->getItems();
	    } else  {
		    $members = "";
	    }
	
		return view('site/pages/pages', array(
            'page_type' => "Pages",
			'side_nav' => $side_nav,
			'category' => $category,			
			'page' => $page,
			'mode' => $mode,
			'side_nav_documents' => $side_nav_documents,
			'members' => $members
        ));

    }
	
	public function getCategory($category_slug){
		$categories = PageCategory::where('slug', '=', $category_slug)->orderBy('position', 'asc')->get();		
		return($categories);
	}
	
	public function getPages($category_id){
		$pages = Page::where('status', '=', 'active')->where('category_id', '=', $category_id)->whereNull('parent_page_id')->orderBy('position', 'asc')->get();		
		
		foreach ($pages as $page):
		   $page['nav_sub'] = $this->getSubPages($page->id);	
		endforeach;
		
		return($pages);
	}
	
	public function getSubPages($page_id){
		$pages = Page::where('status', '=', 'active')->where('parent_page_id', '=', $page_id)->orderBy('position', 'asc')->get();		
		return($pages);
	}	
	
	public function getPage($category_id, $page_slug, $mode){
		if ($mode == "preview") {
		   $pages = Page::where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();	
		} else {
		   $pages = Page::where('status', '=', 'active')->where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();		
		}
		
		return($pages);
	}
}
