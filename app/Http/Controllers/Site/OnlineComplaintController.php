<?php

namespace App\Http\Controllers\Site;

use App\Complaint;
use App\ComplaintAdmin;
use App\ComplaintDeath;
use App\ComplaintDisability;
use App\ComplaintDocument;
use App\Country;
use App\DisabilityType;
use App\Http\Controllers\Controller;
use App\Mail\ComplaintMailUser;
use App\State;
use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use League\Flysystem\Util\MimeType;
use Webpatser\Uuid\Uuid;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;

class OnlineComplaintController extends Controller
{

    public function index()
    {
        return redirect('/');

        //return view('site/online-complaint/online-complaint');
    }

    public function showPersonalInformationCollectionNotice(Request $request)
    {
        return view('site/online-complaint/personal-information-collection-notice',array(
            'request' => $request
        ));
    }

    public function showRegistrationForm(Request $request)
    {
        $countries = Country::all();
        $states = State::all();
        $disability_type = DisabilityType::all();

        return view('site/online-complaint/registration-form',array(
            'request' => $request,
            'countries' => $countries,
            'states' => $states,
            'disability_type' => $disability_type
        ));
    }

    public function saveComplaint(Request $request)
    {

        $rules = array(
            'surname' => 'required',
            'given_names' => 'required',
            'email' => 'required',
            'super_name' => 'required',
            'dec_fullname' => 'required',
            'dec_email' => 'required',
            //'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'surname.required' => 'This field is required.',
            'given_names.required' => 'This field is required.',
            'email.required' => 'This field is required.',
            'super_name.required' => 'This field is required.',
            'dec_fullname.required' => 'This field is required.',
            'dec_email.required' => 'This field is required.',
            //'g-recaptcha-response.required' => 'Please verify yourself',
            //'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('online-complaint/registration-form')->withErrors($validator)->withInput();
        }

        $dob = NULL;
        if($request->dob){
            $dob_arr = explode('/',$request->dob);
            $dob = mktime(0, 0, 0, $dob_arr[1], $dob_arr[0], $dob_arr[2]);
        }

        $current_date = mktime(date("H"),date("i"),date("s"),date("n"),date("j"),date("Y"));

        $fc_officer = 0;
        if($request->fc_officer=='Yes'){
            $fc_officer = 1;
        }else if($request->fc_officer=='No'){
            $fc_officer = 2;
        }

        $fc_response = 0;
        if($request->fc_response=='Yes'){
            $fc_response = 1;
        }else if($request->fc_response=='No'){
            $fc_response = 2;
        }

        $expiry = 0;
        if($request->expiry=='Yes'){
            $expiry = 1;
        }else if($request->expiry=='No'){
            $expiry = 2;
        }

        if($request->c_type=='Death'){
            $c_type = 1;
        }else if($request->c_type=='Disablement'){
            $c_type = 2;
        }else if($request->c_type=='Other'){
            $c_type = 3;
        }

        $uuid = Uuid::generate()->string;

        $complaint = new Complaint();
        $complaint->complaint_made = $fc_officer;
        $complaint->complaint_response = $fc_response;
        $complaint->over_90_days = $expiry;
        $complaint->complaint_type_id = $c_type;
        $complaint->complaint_status_id = 1;
        $complaint->title = $request->title;
        $complaint->surname = $request->surname;
        $complaint->given_name = $request->given_names;
        $complaint->gender = $request->gender;
        $complaint->dob = $dob;
        $complaint->address = $request->postal_address;
        $complaint->state_id = $request->state;
        $complaint->postcode = $request->postcode;
        $complaint->country_id = $request->country;
        $complaint->phone = $request->phone;
        $complaint->mobile = $request->mobile;
        $complaint->email = $request->email;
        $complaint->fax = $request->fax;
        $complaint->on_behalf_name_dob = $request->on_behalf_name_dob;
        $complaint->relationship = $request->relationship;
        $complaint->legal_guardian = $request->legal_guardian;
        $complaint->poa = $request->power_attorney;
        $complaint->super_provider_name = $request->super_name;
        $complaint->super_member_name = $request->member_name;
        $complaint->super_member_policy_number = $request->super_policy;
        $complaint->rep_title = $request->rep_title;
        $complaint->rep_surname = $request->rep_surname;
        $complaint->rep_given_name = $request->rep_given_names;
        $complaint->rep_address = $request->rep_address;
        $complaint->rep_state_id = $request->rep_state;
        $complaint->rep_postcode = $request->rep_postcode;
        $complaint->rep_country_id = $request->rep_country;
        $complaint->rep_phone = $request->rep_telephone;
        $complaint->rep_mobile = $request->rep_mobile;
        $complaint->rep_email = $request->rep_email;
        $complaint->rep_fax = $request->rep_fax;
        $complaint->rep_relationship = $request->rep_relation;
        $complaint->rep_reasons = $request->rep_reasons;
        $complaint->complaint_details = $request->comp_about_details;
        $complaint->complaint_unfair_details = $request->comp_unfair;
        $complaint->complaint_amount_dispute = $request->comp_benefit;
        $complaint->complaint_outcomes = $request->comp_outcomes;
        $complaint->complainant_name = $request->dec_fullname;
        $complaint->complaint_created_date = $current_date;
        $complaint->complaint_receipt_number = $uuid;
        $complaint->save();

        if($request->c_type=='Death'){

            $date_deceased = NULL;
            if($request->date_deceased) {
                $date_deceased_arr = explode('/', $request->date_deceased);
                $date_deceased = mktime(0, 0, 0, $date_deceased_arr[1], $date_deceased_arr[0], $date_deceased_arr[2]);
            }

            $complaint_death = new ComplaintDeath();
            $complaint_death->complaint_id = $complaint->id;
            $complaint_death->surname_deceased = $request->surname_deceased;
            $complaint_death->given_name_deceased = $request->name_deceased;
            $complaint_death->relationship_deceased = $request->relationship_deceased;
            $complaint_death->date_deceased = $date_deceased;
            $complaint_death->save();
        }

        if($request->c_type=='Disablement'){

            $dis_last_date = NULL;
            if($request->dis_last_date) {
                $dis_last_date_arr = explode('/', $request->dis_last_date);
                $dis_last_date = mktime(0, 0, 0, $dis_last_date_arr[1], $dis_last_date_arr[0], $dis_last_date_arr[2]);
            }

            $dis_permanent_cease_date = NULL;
            if($request->dis_permanent_cease_date) {
                $dis_permanent_cease_date_arr = explode('/', $request->dis_permanent_cease_date);
                $dis_permanent_cease_date = mktime(0, 0, 0, $dis_permanent_cease_date_arr[1], $dis_permanent_cease_date_arr[0], $dis_permanent_cease_date_arr[2]);
            }

            $dis_claim_lodge_date = NULL;
            if($request->dis_claim_lodge_date) {
                $dis_claim_lodge_date_arr = explode('/', $request->dis_claim_lodge_date);
                $dis_claim_lodge_date = mktime(0, 0, 0, $dis_claim_lodge_date_arr[1], $dis_claim_lodge_date_arr[0], $dis_claim_lodge_date_arr[2]);
            }

            $dis_pay_start_date = NULL;
            if($request->dis_pay_start_date) {
                $dis_pay_start_date_arr = explode('/', $request->dis_pay_start_date);
                $dis_pay_start_date = mktime(0, 0, 0, $dis_pay_start_date_arr[1], $dis_pay_start_date_arr[0], $dis_pay_start_date_arr[2]);
            }

            $dis_pay_cease_date = NULL;
            if($request->dis_pay_cease_date) {
                $dis_pay_cease_date_arr = explode('/', $request->dis_pay_cease_date);
                $dis_pay_cease_date = mktime(0, 0, 0, $dis_pay_cease_date_arr[1], $dis_pay_cease_date_arr[0], $dis_pay_cease_date_arr[2]);
            }

            $complaint_disability = new ComplaintDisability();
            $complaint_disability->complaint_id = $complaint->id;
            $complaint_disability->dis_type_ids = $request->dis_type;
            $complaint_disability->dis_last_attended_date = $dis_last_date;
            $complaint_disability->dis_ceased_work_date = $dis_permanent_cease_date;
            $complaint_disability->dis_ceased_reasons = $request->dis_reason;
            $complaint_disability->dis_physical_mental = $request->dis_physical_mental;
            $complaint_disability->dis_employer_notice = $request->dis_employer_notice;
            $complaint_disability->dis_claim_lodge_date = $dis_claim_lodge_date;
            $complaint_disability->dis_worker_compensation = $request->dis_worker_compensation;
            $complaint_disability->dis_payment_start_date = $dis_pay_start_date;
            $complaint_disability->dis_payment_ceased_date = $dis_pay_cease_date;
            $complaint_disability->save();
        }

        if($request->c_type=='Other'){

            $complaint_admin = new ComplaintAdmin();
            $complaint_admin->complaint_id = $complaint->id;
            $complaint_admin->all_info_rep_relied = $request->rep_rely;
            $complaint_admin->all_info_rep_expectation = $request->rep_lead;
            $complaint_admin->all_info_rep_how = $request->rep_how;
            $complaint_admin->all_info_rep_action = $request->rep_different;
            $complaint_admin->save();
        }

        return view('site/online-complaint/file-upload',array(
            'complaint_id' => $complaint->id,
            'receipt_number' => $complaint->complaint_receipt_number
        ));

    }

    public function upload(Request $request)
    {
        //ID - receipt number check
        $complaint = Complaint::where('id', $request->id)->first();
        if($complaint->complaint_receipt_number != $request->receipt_number){
            return Response::json(['files' => []]);
        }

        $files_data = array();
        $files = $request->file('files');
        $i=0;
        foreach($files as $file){

            $file_name = snake_case($file->getClientOriginalName());

            $document = new ComplaintDocument();
            $document->complaint_id = $complaint->id;
            $document->complaint_document = $file_name;
            $document->document_title = $request->title[$i];
            $document->document_description = $request->description[$i];
            $document->document_date = date('Y-m-d H:i:s');
            $document->deleted = 0;
            $document->save();

            $files_data[] = [
                'name' => $file_name,
                'size' => $file->getSize(),
                'type' => $file->getMimeType(),
                'title' => $request->title,
                'description' => $request->description,
                'deleteType' => 'POST',
                'deleteUrl' => url('online-complaint/'.$document->id.'/delete'),
                'url' => url('online-complaint/'.$document->id.'/download'),
                'upload_to_db' => true
            ];

            Storage::putFileAs('complaint/'.$complaint->id, $file, $file_name);
            $i++;
        }

        return Response::json(['files' => $files_data]);
    }

    public function download($document_id)
    {
        $document = ComplaintDocument::where('id','=',$document_id)->first();
        $file = Storage::get('complaint/'.$document->complaint_id.'/'.$document->complaint_document);
        $type = MimeType::detectByContent($file);

        $headers = [
            'Content-Type' => $type
        ];

        return response($file,200,$headers);
    }

    public function delete($document_id)
    {
        $document = ComplaintDocument::where('id','=',$document_id)->first();
        Storage::delete('complaint/'.$document->complaint_id.'/'.$document->complaint_document);
        $document->delete();
        return Response::json(['status' => 'success'],200);
    }

    public function success(Request $request)
    {
        //ID - receipt number check
        $complaint = Complaint::where('id', $request->id)->first();
        if($complaint->complaint_receipt_number != $request->receipt_number){
            return redirect('online-complaint');
        }

        $complaint->is_finished = true;
        $complaint->save();

        Mail::to($complaint->email)->send(new ComplaintMailUser($complaint));

        return view('site/online-complaint/success',array(
            'complaint' => $complaint
        ));

    }

    public function pdf($complaint_receipt_number)
    {
        $complaint = Complaint::where('complaint_receipt_number', $complaint_receipt_number)->first();
        $pdf = PDF::loadView('site/online-complaint/pdf', compact('complaint'));
        return $pdf->download($complaint->complaint_receipt_number.'.pdf');
    }

    public function testPdf($complaint_receipt_number)
    {
        $complaint = Complaint::where('complaint_receipt_number', $complaint_receipt_number)->first();
        return view('site/online-complaint/pdf',array(
            'complaint' => $complaint
        ));
    }


}
