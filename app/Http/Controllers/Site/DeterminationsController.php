<?php

namespace App\Http\Controllers\Site;

use App\Determination;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Util\MimeType;
use TeamTNT\TNTSearch\TNTSearch;

class DeterminationsController extends Controller
{
    public function index(){

		return view('site/determinations/search');
    }

    public function results(TNTSearch $tnt, Request $request)
    {

        $sort = false;

        if($request->get('query')=='' && ($request->year=='All' || $request->year==null)){

            $sort = true;

            $results = Determination::orderBy('determination_date', 'desc')->paginate(25);

        }else if($request->get('query')==''  && ($request->year!='All' || $request->year!=null)){

            $results = Determination::where('determination_year','=', $request->year)->orderBy('determination_date', 'desc')->paginate(25);

        }else {

            if ($request->year == 'All' || $request->year == null) {

                $sort = true;

                if($request->sort_by == 'date'){

                    $results = Determination::search($request->get('query'))
                        ->where('status', 'active')
                        ->where('is_deleted', 'false')
                        ->get();

                    $results = $results->sortBy('determination_year')->reverse();
                    $results = $this->arrayPaginate($results, 25);
                    $results->withPath('results');

                }else{

                    $results = Determination::search($request->get('query'))
                        ->where('status', 'active')
                        ->where('is_deleted', 'false')
                        ->paginate(25);
                }

            } else {

                $results = Determination::search($request->get('query'))
                    ->where('determination_year', $request->year)
                    ->where('status', 'active')
                    ->where('is_deleted', 'false')
                    ->paginate(25);
            }
        }

        return view('site/determinations/results', array(
            'query' => $request->get('query'),
            'year' => $request->year,
            'results' => $results,
            'sort' => $sort
        ));
    }

    public function detail($determination_id)
    {

        $determination = Determination::where('id','=',$determination_id)->first();

        if($determination->description == 'User provided AUSTLII File'){

            $html_file = Storage::get('determinations/content/'.$determination->austlii_file);
            $regex = '#<\s*?body\b[^>]*>(.*?)</body\b[^>]*>#s';
            preg_match($regex, $html_file, $matches);
            $description = str_replace('<hr>','', $matches[1]);
            $determination->description = $description;
        }

        return view('site/determinations/detail', array(
            'determination' => $determination
        ));
    }

    public function download($determination_id)
    {
        $determination = Determination::where('id','=',$determination_id)->first();
        $file = Storage::get('determinations/'.$determination->determination_file);
        $type = MimeType::detectByContent($file);

        $headers = [
            'Content-Type' => $type
        ];

        return response($file,200,$headers);
    }

    public function arrayPaginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
