<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Member;

class MembersController extends Controller
{
    public function index(){
		$items = $this->getItems();		
		
		return view('site/members/list', array(         						
			'items' => $items,				
        ));

    }
		
	public function getItems(){		
		$items = Member::where('status', '=', 'active')->orderBy('position', 'desc')->get();
		return($items);
	}		
}
