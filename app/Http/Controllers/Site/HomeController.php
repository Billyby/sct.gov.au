<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

    	return view('site/index');
    }

    public function subThankYou()
    {
        return view('site/subscribe/subscribe-thank-you');
    }

    public function subConfirm()
    {
        return view('site/subscribe/subscribe-confirm');
    }

}
