<?php

namespace App\Http\Controllers\Site;

use App\Document;
use App\Faq;
use App\Http\Controllers\Controller;
use App\News;
use App\Page;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class SiteSearchController extends Controller
{

    public function index(Request $request){

        $page_results = Page::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $news_results = News::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $document_results = Document::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $faq_results = Faq::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $highest = max([
            count($news_results),
            count($page_results),
            count($document_results),
            count($faq_results)
        ]);

        $results = array();

        for($i=0; $i<$highest; $i++){

            if(!empty($page_results[$i])){
                $page = [
                    'type' => 'page',
                    'item' => $page_results[$i]
                ];
                $results[] = $page;
            }

            if(!empty($news_results[$i])){
                $news = [
                    'type' => 'news',
                    'item' => $news_results[$i]
                ];
                $results[] = $news;
            }

            if(!empty($document_results[$i])){
                $document = [
                    'type' => 'document',
                    'item' => $document_results[$i]
                ];
                $results[] = $document;
            }

            if(!empty($faq_results[$i])){
                $faq = [
                    'type' => 'faq',
                    'item' => $faq_results[$i]
                ];
                $results[] = $faq;
            }
        }

        $results = $this->paginate($results, [
            'path' => 'results?query='.$request->get('query')
        ]);

        return view('site/site-search/results', array(
            'query' => $request->get('query'),
            'results' => $results
        ));
    }

    public function paginate($items, $options = [], $perPage = 25, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
