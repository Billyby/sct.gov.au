<?php

namespace App\Console\Commands;

use App\Determination;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SetDerterminationYear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'determinations:setyear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Copying year column...');

        $determinations = \DB::table('determinations')->get();

        $bar = $this->output->createProgressBar($determinations->count());

        foreach($determinations as $determination){

            if($determination->determination_date!=NULL){

                \DB::table('determinations')
                    ->where('id', $determination->id)
                    ->update(['determination_year' => Carbon::createFromTimestamp($determination->determination_date)->year]);
            }

            $bar->advance();
        }

        $bar->finish();
        $this->info("\nFinished!");
    }
}
