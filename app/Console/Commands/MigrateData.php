<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sctdata:migrate';
    protected $description = 'Migrates all data from current database to new';

    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        /*
        $this->info('Migrating "determinations" table');
        $determinations = \DB::connection('sctoldlive')->table('determinations')->get();

        \DB::connection('sctnewprod')->table('determinations')->truncate();

        $bar = $this->output->createProgressBar($determinations->count());

        foreach($determinations as $determination){

            $status = 'active';
            if($determination->live==0){
                $status = 'passive';
            }

            \DB::connection('sctnewprod')->table('determinations')->insert([
                'id' => $determination->id,
                'category_id' => $determination->category_id,
                'determination_number' => $determination->determinationNumber,
                'file_number' => $determination->fileNumber,
                'description' => $determination->description,
                'austlii_file' => $determination->austliiFile,
                'summary' => $determination->summary,
                'determination_file' => $determination->determinationFile,
                'determination_date' => $determination->determinationDate,
                'status' => $status,
                'cms_user_id' => $determination->cms_user_id,
                'created_at' => $determination->modified,
                'updated_at' => $determination->modified
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");

        /////////////////////////////////////////

        $this->info('Migrating "determination_dates" table');
        $determination_dates = \DB::connection('sctoldlive')->table('determinations_dates')->get();

        \DB::connection('sctnewprod')->table('determination_dates')->truncate();

        $bar = $this->output->createProgressBar($determination_dates->count());

        foreach($determination_dates as $determination_date){

            \DB::connection('sctnewprod')->table('determination_dates')->insert([
                'determination_number' => $determination_date->determinationNumber,
                'date' => $determination_date->date,
                'date_unix' => $determination_date->dateUnix,
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");
*/
        /////////////////////////////////////////
        /*
        $this->info('Migrating "complaints" table');
        $complaints = \DB::connection('sctoldlive')->table('tblcomplaints')->where('ComplaintID','>',3536)->get();

       // \DB::connection('sctnewprod')->table('complaints')->truncate();

        $bar = $this->output->createProgressBar($complaints->count());

        foreach($complaints as $complaint){


            $complaint_created_date = NULL;
            if($complaint->ComplaintCreatedDate!=0){
                $complaint_created_date = date('Y-m-d H:i:s',$complaint->ComplaintCreatedDate);
            }

            $complaint_actioned_date = NULL;
            if($complaint->ComplaintActionedDate!=0){
                $complaint_actioned_date = date('Y-m-d H:i:s',$complaint->ComplaintActionedDate);
            }

            if($complaint_created_date=='2017-10-01 02:35:28'){
                $complaint_created_date = '2017-10-02 02:35:28';
            }

            \DB::connection('sctnewprod')->table('complaints')->insert([
                'id' => $complaint->ComplaintID,
                'complaint_made' => $complaint->ComplaintMade,
                'complaint_response' => $complaint->ComplaintResponse,
                'over_90_days' => $complaint->Over90Days,
                'complaint_type_id' => $complaint->ComplaintTypeID,
                'complaint_status_id' => $complaint->ComplaintStatusID,
                'title' => $complaint->Title,
                'surname' => $complaint->Surname,
                'given_name' => $complaint->GivenName,
                'gender' => $complaint->Gender,
                'dob' => $complaint->DOB,
                'address' => $complaint->Address,
                'state_id' => $complaint->StateID,
                'postcode' => $complaint->Postcode,
                'country_id' => $complaint->CountryID,
                'phone' => $complaint->Phone,
                'mobile' => $complaint->Mobile,
                'email' => $complaint->Email,
                'fax' => $complaint->Fax,
                'on_behalf_name_dob' => $complaint->OnBehalfNameDOB,
                'relationship' => $complaint->Relationship,
                'legal_guardian' => $complaint->LegalGuardian,
                'poa' => $complaint->POA,
                'super_provider_name' => $complaint->SuperProviderName,
                'super_member_name' => $complaint->SuperMemberName,
                'super_member_policy_number' => $complaint->SuperMemberPolicyNumber,
                'rep_title' => $complaint->RepTitle,
                'rep_surname' => $complaint->RepSurname,
                'rep_given_name' => $complaint->RepGivenName,
                'rep_address' => $complaint->RepAddress,
                'rep_state_id' => $complaint->RepStateID,
                'rep_postcode' => $complaint->RepPostcode,
                'rep_country_id' => $complaint->RepCountryID,
                'rep_phone' => $complaint->RepPhone,
                'rep_mobile' => $complaint->RepMobile,
                'rep_email' => $complaint->RepEmail,
                'rep_fax' => $complaint->RepFax,
                'rep_relationship' => $complaint->RepRelationship,
                'rep_reasons' => $complaint->RepReasons,
                'complaint_details' => $complaint->ComplaintDetails,
                'complaint_unfair_details' => $complaint->ComplaintUnfairDetails,
                'complaint_amount_dispute' => $complaint->ComplaintAmountDispute,
                'complaint_outcomes' => $complaint->ComplaintOutcomes,
                'complainant_name' => $complaint->ComplainantName,
                'complaint_submitted_date' => $complaint->ComplaintSubmittedDate,
                'complaint_created_date' => $complaint->ComplaintCreatedDate,
                'complaint_reviewed_date' => $complaint->ComplaintReviewedDate,
                'complaint_actioned_date' => $complaint->ComplaintActionedDate,
                'actioned' => $complaint->Actioned,
                'complaint_receipt_number' => $complaint->ComplaintReceiptNumber,
                'notes' => $complaint->Notes,
                'modified' => $complaint->modified,
                'cms_user_id' => $complaint->cms_user_id,
                'is_finished' => 'true',
                'created_at' => $complaint_created_date,
                'updated_at' => $complaint_actioned_date
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");


        /////////////////////////////////////////

        $this->info('Migrating "complaint_admin" table');
        $admins = \DB::connection('sctoldlive')->table('tblcomplaintadmin')->where('ComplaintID','>',3536)->get();

        //\DB::connection('sctnewprod')->table('complaint_admin')->truncate();

        $bar = $this->output->createProgressBar($admins->count());

        foreach($admins as $admin){

            \DB::connection('sctnewprod')->table('complaint_admin')->insert([
                'id' => $admin->ComplaintAdminID,
                'complaint_id' => $admin->ComplaintID,
                'all_info_rep_relied' => $admin->AllInfoRepRelied,
                'all_info_rep_expectation' => $admin->AllInfoRepExpectation,
                'all_info_rep_how' => $admin->AllInfoRepHow,
                'all_info_rep_action' => $admin->AllInfoRepAction,
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");

        /////////////////////////////////////////

        $this->info('Migrating "complaint_deaths" table');
        $deaths = \DB::connection('sctoldlive')->table('tblcomplaintdeaths')->where('ComplaintID','>',3536)->get();

        //\DB::connection('sctnewprod')->table('complaint_deaths')->truncate();

        $bar = $this->output->createProgressBar($deaths->count());

        foreach($deaths as $death){

            \DB::connection('sctnewprod')->table('complaint_deaths')->insert([
                'id' => $death->ComplaintDeathID,
                'complaint_id' => $death->ComplaintID,
                'surname_deceased' => $death->SurnameDeceased,
                'given_name_deceased' => $death->GivenNameDeceased,
                'relationship_deceased' => $death->RelationshipDeceased,
                'date_deceased' => $death->	DateDeceased
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");

        /////////////////////////////////////////

        $this->info('Migrating "complaint_disabilities" table');
        $disabilities = \DB::connection('sctoldlive')->table('tblcomplaintdisabilities')->where('ComplaintID','>',3536)->get();

        //\DB::connection('sctnewprod')->table('complaint_disabilities')->truncate();

        $bar = $this->output->createProgressBar($disabilities->count());

        foreach($disabilities as $disability){

            \DB::connection('sctnewprod')->table('complaint_disabilities')->insert([
                'id' => $disability->ComplaintDisabilityID,
                'complaint_id' => $disability->ComplaintID,
                'dis_type_ids' => $disability->DisTypeIDs,
                'dis_last_attended_date' => $disability->DisLastAttendedDate,
                'dis_ceased_work_date' => $disability->DisCeasedWorkDate,
                'dis_ceased_reasons' => $disability->DisCeasedReasons,
                'dis_physical_mental' => $disability->DisPhysicalMental,
                'dis_employer_notice' => $disability->DisEmployerNotice,
                'dis_claim_lodge_date' => $disability->DisClaimLodgeDate,
                'dis_worker_compensation' => $disability->DisWorkerCompensation,
                'dis_payment_start_date' => $disability->DisPaymentStartDate,
                'dis_payment_ceased_date' => $disability->DisPaymentCeasedDate
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");

        /////////////////////////////////////////

        $this->info('Migrating "complaint_documents" table');
        $disabilities = \DB::connection('sctoldlive')->table('tblcomplaintdocuments')->where('ComplaintID','>',3536)->get();

        //\DB::connection('sctnewprod')->table('complaint_documents')->truncate();

        $bar = $this->output->createProgressBar($disabilities->count());

        foreach($disabilities as $disability){

            \DB::connection('sctnewprod')->table('complaint_documents')->insert([
                'id' => $disability->ComplaintDocumentID,
                'complaint_id' => $disability->ComplaintID,
                'complaint_document' => $disability->ComplaintDocument,
                'document_title' => $disability->DocumentTitle,
                'document_description' => $disability->DocumentDescription,
                'document_date' => $disability->DocumentDate,
                'deleted' => $disability->Deleted,
                'created_at' => $disability->DocumentDate,
                'updated_at' => $disability->DocumentDate
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n");*/
    }
}
