<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintDisability extends Model
{	
    protected $table = 'complaint_disabilities';

    public function type()
    {
        return $this->belongsTo(DisabilityType::class, 'dis_type_ids');
    }

    public function getDisLastAttendedDateAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }

    public function getDisCeasedWorkDateAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }

    public function getDisPhysicalMentalAttribute($value)
    {
        if($value==1){
            return 'Yes';
        }else if($value==2){
            return 'No';
        }else{
            return 'N/A';
        }
    }

    public function getDisEmployerNoticeAttribute($value)
    {
        if($value==1){
            return 'Yes';
        }else if($value==2){
            return 'No';
        }else{
            return 'N/A';
        }
    }

    public function getDisClaimLodgeDateAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }

    public function getDisWorkerCompensationAttribute($value)
    {
        if($value==1){
            return 'Yes';
        }else if($value==2){
            return 'No';
        }else{
            return 'N/A';
        }
    }

    public function getDisPaymentStartDateAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }

    public function getDisPaymentCeasedDateAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }

}
