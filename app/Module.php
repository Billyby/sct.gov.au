<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;
use Spatie\Permission\Models\Permission;

class Module extends Model
{
	use SortableTrait;

    protected $table = 'modules';

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'module_id');
    }
}
