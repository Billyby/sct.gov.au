<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;
use Laravel\Scout\Searchable;

class Document extends Model
{
    use SortableTrait, Sortable, Searchable;

    protected $table = 'documents';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    public function category()
    {
        return $this->belongsTo(DocumentCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(DocumentCategory::class,'id','category_id');
    }

    public function getDescriptionCleanAttribute()
    {
        $description = strip_tags($this->attributes['description']);
        $description = str_replace('&nbsp;','', $description);
        $description = str_replace("\r\n",'', $description);
        $description = str_replace("\t",'', $description);
        $description = str_replace("&quot;",'"', $description);
        $description = str_replace("&#39;","'", $description);
        $description = str_replace("&lt;","<", $description);
        $description = str_replace("&gt;",">", $description);
        $description = str_replace("&amp;",'&', $description);
        $description = str_limit($description, 320);

        return $description;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('documents-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
