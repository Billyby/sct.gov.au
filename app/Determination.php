<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Scout\Searchable;

class Determination extends Model
{
    use Sortable, Searchable;

    protected $table = 'determinations';

    public $sortable = ['id', 'category_id', 'determination_number', 'determination_date', 'status'];

    /*public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }*/

    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    public function category()
    {
        return $this->belongsTo(DeterminationCategory::class, 'category_id');
    }

	public function categoryactive()
    {
        return $this->belongsTo(DeterminationCategory::class, 'category_id')->where('status', '=', 'active');
    }

    public function categorysort()
    {
        return $this->hasOne(DeterminationCategory::class,'id','category_id');
    }

    public function getDescriptionCleanAttribute()
    {
        $description = strip_tags($this->attributes['description']);
        $description = str_replace('&nbsp;','', $description);
        $description = str_replace("\r\n",'', $description);
        $description = str_replace("\t",'', $description);
        $description = str_replace("&quot;",'"', $description);
        $description = str_replace("&#39;","'", $description);
        $description = str_replace("&lt;","<", $description);
        $description = str_replace("&gt;",">", $description);
        $description = str_replace("&amp;",'&', $description);
        $description = str_limit($description, 320);

        return $description;
    }

    public function getDescriptionDetailAttribute()
    {
        $description = $this->attributes['description'];
        $description = str_replace('&nbsp;','', $description);
        $description = str_replace("\r\n",'', $description);
        $description = str_replace("\t",'', $description);
        $description = str_replace("&quot;",'"', $description);
        $description = str_replace("&#39;","'", $description);
        $description = str_replace("&lt;","<", $description);
        $description = str_replace("&gt;",">", $description);
        $description = str_replace("&amp;",'&', $description);
        $description = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $description);
        return $description;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('determinations-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){

            $select =  $query->where('determination_number','like', '%'.addslashes($filter['search']).'%')
                ->orWhere('file_number','like', '%'.addslashes($filter['search']).'%')
                ->orWhere('description','like', '%'.addslashes($filter['search']).'%')
                ->orWhere('austlii_file','like', '%'.addslashes($filter['search']).'%')
                ->orWhere('summary','like', '%'.addslashes($filter['search']).'%')
                ->orWhere('determination_file','like', '%'.addslashes($filter['search']).'%');
        }

        return $select;
    }
}
