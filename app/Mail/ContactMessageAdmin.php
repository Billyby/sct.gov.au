<?php

namespace App\Mail;

use App\ContactMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $contact_message;

    public function __construct(ContactMessages $contact_message)
    {
        $this->contact_message = $contact_message;
    }

    public function build()
    {
        return $this->from('info@sct.gov.au')
            ->subject('SCT - New Contact Message')
            ->view('site/emails/contact-message-admin');
    }
}
