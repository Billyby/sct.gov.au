<?php

namespace App\Mail;

use App\ContactMessages;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $contact_message;
    public $mail_content;

    public function __construct(ContactMessages $contact_message)
    {
        $this->contact_message = $contact_message;

        $setting = Setting::where('key','=','contact-details')->first();
        $this->mail_content = $setting->value;
    }

    public function build()
    {
        return $this->from('info@sct.gov.au')
            ->subject('Superannuation Complaints Tribunal')
            ->view('site/emails/contact-message-user');
    }
}
