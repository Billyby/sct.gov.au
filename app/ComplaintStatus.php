<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintStatus extends Model
{	
    protected $table = 'complaint_status';

    public function complaints()
    {
        return $this->hasMany(Complaint::class, 'complaint_status_id');
    }
}
