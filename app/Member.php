<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Member extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'members';

    public $sortable = ['firstName', 'lastName', 'email', 'suburb', 'type_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function type()
    {
        return $this->belongsTo(MemberType::class, 'type_id');
    }

    public function typesort()
    {
        return $this->hasOne(MemberType::class,'id','type_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('members-filter');
        $select = "";

        if($filter['type'] && $filter['type']!="all"){
            $select =  $query->where('type_id', $filter['type']);
        }

        if($filter['search']){
            $select =  $query
				            ->where('firstName','like', '%'.$filter['search'].'%')
				            ->orWhere('lastName','like', '%'.$filter['search'].'%');
				            ;
        }

        return $select;
    }
}
