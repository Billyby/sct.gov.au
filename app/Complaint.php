<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Complaint extends Model
{
    use Sortable;

    protected $table = 'complaints';

    public $sortable = ['given_name', 'surname', 'complaint_type_id', 'created_at', 'complaint_status_id'];

    public function documents()
    {
        return $this->hasMany(ComplaintDocument::class, 'complaint_id');
    }

    public function type()
    {
        return $this->belongsTo(ComplaintType::class, 'complaint_type_id');
    }

    public function typesort()
    {
        return $this->hasOne(ComplaintType::class,'id','complaint_type_id');
    }

    public function status()
    {
        return $this->belongsTo(ComplaintStatus::class, 'complaint_status_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function repState()
    {
        return $this->belongsTo(State::class, 'rep_state_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function repCountry()
    {
        return $this->belongsTo(Country::class, 'rep_country_id');
    }

    public function death()
    {
        return $this->hasOne(ComplaintDeath::class, 'complaint_id');
    }

    public function disability()
    {
        return $this->hasOne(ComplaintDisability::class, 'complaint_id');
    }

    public function other()
    {
        return $this->hasOne(ComplaintAdmin::class, 'complaint_id');
    }

    public function scopeFilter($query)
    {
        $filter = session()->get('complaints-filter');
        $select = "";

        if($filter['type'] && $filter['type']!="all"){
            $select =  $query->where('complaint_type_id', $filter['type']);
        }

        if($filter['search']){
            $select =  $query->where('surname','like', '%'.$filter['search'].'%')
                ->orWhere('given_name','like', '%'.$filter['search'].'%')
                ->orWhere('notes','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getGenderAttribute($value)
    {
        if($value==1){
            return 'Male';
        }else if($value==2){
            return 'Female';
        }else{
            return 'N/A';
        }
    }

    public function getDobAttribute($value)
    {
        if($value==null){
            return '';
        }

        return date('d/m/Y', $value);
    }

    public function getLegalGuardianAttribute($value)
    {
        if($value==1){
            return 'Yes';
        }else if($value==2){
            return 'No';
        }else{
            return 'N/A';
        }
    }

    public function getPoaAttribute($value)
    {
        if($value==1){
            return 'Yes';
        }else if($value==2){
            return 'No';
        }else{
            return 'N/A';
        }
    }

    public function getComplaintReviewedDateAttribute($value)
    {
        if($value==0){
            return '';
        }

        return date('d/m/Y', $value);
    }
}
