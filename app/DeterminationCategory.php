<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class DeterminationCategory extends Model
{
    use SortableTrait;

    protected $table = 'determination_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function determinations()
    {
        return $this->hasMany(Determination::class, 'category_id')->where('status', '=', 'active');
    }
}
