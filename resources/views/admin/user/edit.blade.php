@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $user->name ." - ".$user->email }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/user') }}"><i class="fa fa-users"></i> Users</a></li>
                <li><a href="#">Edit User</a></li>
            </ol>
        </section>

        <section class="content">
            @can('edit-permission')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">User Permissions</h3>
                                <span class="pull-right">
                                    <button id="select-all" class="btn btn-info btn-sm">Select All
                                        <i class="fa fa-check"></i>
                                    </button>
                                </span>
                            </div>
                            <form method="post" class="form-horizontal"
                                  action="{{ url('dreamcms/user/save-permission') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="box-body container-fluid">
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 module-permission-container">
                                        <h4>Users</h4>
                                        @foreach($user_permissions as $user_permission)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="minimal" name="permissions[]"
                                                           value="{{ $user_permission->name }}" {!! ($user_permission->user_has) ? ' checked' : null !!}>
                                                    {{ $user_permission->translation }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 module-permission-container">
                                        <h4>Settings</h4>
                                        @foreach($settings_permissions as $settings_permission)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="minimal" name="permissions[]"
                                                           value="{{ $settings_permission->name }}" {!! ($settings_permission->user_has) ? ' checked' : null !!}>
                                                    {{ $settings_permission->translation }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 module-permission-container">
                                        <h4>Complaints</h4>
                                        @foreach($complaint_permissions as $complaint_permission)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="minimal" name="permissions[]"
                                                           value="{{ $complaint_permission->name }}" {!! ($complaint_permission->user_has) ? ' checked' : null !!}>
                                                    {{ $complaint_permission->translation }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>

                                    @foreach($module_permissions as $module)
                                        @if(count($module->permissions)>0 && $module->status=='active')
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 module-permission-container">
                                                <h4>{{ $module->display_name }}</h4>
                                                @foreach($module->permissions as $permission)
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="minimal" name="permissions[]"
                                                                   value="{{ $permission->name }}" {!! ($permission->user_has) ? ' checked' : null !!}>
                                                            {{ $permission->translation }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endforeach

                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endcan

            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Change Information</h3>
                        </div>
                        <form method="post" class="form-horizontal"
                              action="{{ url('dreamcms/user/save-information') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{ old('name',$user->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="email" value="{{ old('email',$user->email) }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @can('user-change-password')
                <div class="row">
                    <div class="col-sm-12 col-md-10 col-lg-8">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Change Password</h3>
                            </div>
                            <form method="post" class="form-horizontal"
                                  action="{{ url('dreamcms/user/save-password') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="box-body">
                                    <div class="form-group{{ ($errors->has('password')) ? ' has-error' : '' }}">
                                        <label class="col-sm-3 control-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="password">
                                            @if ($errors->has('password'))
                                                <small class="help-block">{{ $errors->first('password') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group {{ ($errors->has('password_confirmation')) ? ' has-error' : '' }}">
                                        <label class="col-sm-3 control-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="password_confirmation">
                                            @if ($errors->has('password_confirmation'))
                                                <small class="help-block">{{ $errors->first('password_confirmation') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endcan
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var cont = 0;

            $('#select-all').click(function(){
                if(cont==0){
                    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck('check');
                    cont=1;
                }else if(cont==1){
                    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck('uncheck');
                    cont=0;
                }

            });
        });
    </script>
@endsection
