<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">

            @can('view-complaints')
            <li {!! Request::segment(2) == 'complaints' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/complaints') }}"><i class="fa fa-exclamation-circle"></i> <span>Complaints</span></a></li>
            @endcan

            <!-- Contact Module -->
            @if($contact_status=='active')
                @can('view-contact')
                    <li class="treeview{!! Request::segment(2) == 'contact' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-envelope"></i> <span>Contact</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! (Request::segment(2) == 'form-builder') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/contact/form-builder') }}"><i class="fas fa-circle"></i> Form Builder</a></li>
                            <li {!! (Request::segment(2) == 'contact') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/contact') }}"><i class="fas fa-circle"></i> Inbox</a></li>
                            @can('view-edit-details')
                            <li {!! Request::segment(3) == 'details' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/contact/details') }}"><i class="fas fa-circle"></i> Details</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Determinations Module -->
            @if($determinations_status=='active')
                @can('view-determinations')
                    <li class="treeview{!! Request::segment(2) == 'determinations' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-file"></i> <span>Determinations</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/determinations/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'determinations') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/determinations') }}"><i class="fas fa-circle"></i> All Determinations</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Documents Module -->
            @if($documents_status=='active')
                @can('view-documents')
                    <li class="treeview{!! Request::segment(2) == 'documents' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-sticky-note"></i> <span>Documents</span>
                            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/documents/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'documents') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/documents') }}"><i class="fas fa-circle"></i> All Documents</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- FAQs Module -->
            @if($faqs_status=='active')
                @can('view-faqs')
                    <li class="treeview{!! Request::segment(2) == 'faqs' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-question-circle"></i> <span>FAQS</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/faqs/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'faqs') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/faqs') }}"><i class="fas fa-circle"></i> All FAQS</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Gallery Module -->
            @if($gallery_status=='active')
                @can('view-gallery')
                    <li class="treeview{!! Request::segment(2) == 'gallery' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-images"></i> <span>Gallery</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/gallery/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'gallery') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/gallery') }}"><i class="fas fa-circle"></i> All Images</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Members Module -->
            @if($members_status=='active')
                @can('view-members')
                    <li class="treeview{!! Request::segment(2) == 'members' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-users"></i> <span>Members</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'types' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/members/types') }}"><i class="fas fa-circle"></i> Types</a></li>
                            <li {!! (Request::segment(2) == 'members') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/members') }}"><i class="fas fa-circle"></i> All Members</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- News Module -->
            @if($news_status=='active')
                @can('view-news')
                    <li class="treeview{!! Request::segment(2) == 'news' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="far fa-newspaper"></i> <span>News</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/news/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'news') && (Request::segment(2) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/news') }}"><i class="fas fa-circle"></i> All News</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Pages Module -->
            @if($pages_status=='active')
            @can('view-pages')
            <li class="treeview{!! Request::segment(2) == 'pages' ? ' menu-open active' : null !!}">
                <a href="#">
                    <i class="fas fa-file-alt"></i> <span>Pages</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/pages/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                    <li {!! (Request::segment(2) == 'pages') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/pages') }}"><i class="fas fa-circle"></i> All Pages</a></li>
                </ul>
            </li>
            @endcan
            @endif
            
            <!-- Projects Module -->
            @if($projects_status=='active')
                @can('view-projects')
                    <li class="treeview{!! Request::segment(2) == 'projects' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-clipboard"></i> <span>Projects</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/projects/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'projects') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/projects') }}"><i class="fas fa-circle"></i> All Projects</a></li>
                        </ul>
                    </li>
                @endcan
            @endif


            <li class="header"></li>

            @if(Auth::user()->role=='admin')
            <li {!! Request::segment(2) == 'modules' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/modules') }}"><i class="fa fa-cubes"></i> <span>Modules</span></a></li>
            @endif

            @can('view-user')
            <li {!! Request::segment(2) == 'user' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
            @endcan

            @can('view-settings')
            <li class="treeview{!! Request::segment(2) == 'settings' ? ' menu-open active' : null !!}">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('edit-general')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings') }}"><i class="fas fa-circle"></i> General</a></li>
                    @endcan
                    @can('edit-home-page')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'home-page') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/home-page') }}"><i class="fas fa-circle"></i> Home Page</a></li>
                    @endcan
                    @can('edit-header-images')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'header-images') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/header-images') }}"><i class="fas fa-circle"></i> Header Images</a></li>
                    @endcan
                    @can('edit-contact-details')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'contacts') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/contacts') }}"><i class="fas fa-circle"></i> Contact Details</a></li>
                    @endcan
                    @can('edit-navigation')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'navigation') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/navigation') }}"><i class="fas fa-circle"></i> Navigation</a></li>
                    @endcan
                    @can('edit-home-page-slider')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'home-sliders') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/home-sliders') }}"><i class="fas fa-circle"></i> Home Page Slider</a></li>
                    @endcan
                    @can('edit-social-media')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'social-media') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/social-media') }}"><i class="fas fa-circle"></i> Social Media</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            
            <li {!! Request::segment(2) == 'media-library' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/media-library') }}"><i class="fas fa-file"></i> <span>Media Library</span></a></li>

        </ul>
    </section>
</aside>