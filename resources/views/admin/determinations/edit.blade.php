@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Determinations</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/determinations') }}"><i class="fas fa-file"></i> Determinations</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Determinations</h3>
                        </div>

                        <form method="post" class="form-horizontal" enctype="multipart/form-data"
                              action="{{ url('dreamcms/determinations/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $determination->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('determination_number')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Determination Number *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="determination_number"
                                               id="determination_number" placeholder="Determination Number"
                                               value="{{ old('determination_number',$determination->determination_number) }}">
                                        @if ($errors->has('determination_number'))
                                            <small class="help-block">{{ $errors->first('determination_number') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('file_number')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">File Number *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="file_number" id="file_number"
                                               placeholder="File Number" value="{{ old('file_number',$determination->file_number) }}">
                                        @if ($errors->has('file_number'))
                                            <small class="help-block">{{ $errors->first('file_number') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                if(old('category_id')!=''){
                                    $category_id = old('category_id');
                                }else{
                                    $category_id = $determination->category_id;
                                }
                                @endphp
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}" id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>

                                    <div class="col-sm-10{{ ($errors->has('category_id')) ? ' has-error' : '' }}">
                                        @if(count($categories)>0)
                                            <select name="category_id" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <div class="callout callout-danger">
                                                <h4>No category found!</h4>
                                                <a href="{{ url('dreamcms/determinations/add-category') }}">Please click here to add category</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Determination Full Text *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" id="description"
                                                  name="description" maxlength="250"
                                                  placeholder="Determination Full Text">{{ old('description', $determination->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Current Determination File</label>
                                    <div class="col-sm-10"><a href="{{ url('dreamcms/determinations/'.$determination->id.'/download') }}" target="_blank">{{ $determination->determination_file }}</a></div>
                                </div>

                                <div class="form-group {{ ($errors->has('determination_file')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Update Determination File</label>

                                    <div class="col-sm-10">
                                        <input type="file" name="determination_file" id="determination_file">
                                        @if ($errors->has('determination_file'))
                                            <small class="help-block">{{ $errors->first('determination_file') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('summary')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Summary</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="summary"
                                                  placeholder="Short Description">{{ old('summary', $determination->summary) }}</textarea>
                                        @if ($errors->has('summary'))
                                            <small class="help-block">{{ $errors->first('summary') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('determination_date')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Determination Date *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="determination_date" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ old('determination_date', date('d/m/Y', $determination->determination_date)) }}">
                                        </div>
                                        @if ($errors->has('determination_date'))
                                            <small class="help-block">{{ $errors->first('determination_date') }}</small>
                                        @endif
                                    </div>
                                </div>
                                @php
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }else{
                                        $status = $determination->status;
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/determinations') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save & Close</button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            CKEDITOR.replace('description');
            CKEDITOR.replace('summary');
            $(".select2").select2();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    </script>
@endsection 