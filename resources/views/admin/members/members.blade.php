@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Members</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> Members</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/members') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <select name="type" class=" select2" style="width: 100%;">
                                    <option value="all" {{ $session['type'] == "" || $session['type']=="all" ? ' selected="selected"' : '' }}>
                                        All Types
                                    </option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}"{{ $session['type'] == $type->id ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/members/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                        </div>
                    </form>

                    <div class="pull-right box-tools">

                        @can('sort-member')
                            <a href="{{ url('dreamcms/members/sort') }}" type="button" class="btn bg-olive btn-sm"
                               data-widget="add">Sort
                                <i class="fa fa-list-ul"></i>
                            </a>
                        @endcan

                        @can('add-members')
                        <a href="{{ url('dreamcms/members/add') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body">
                    @if(count($members))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('name')</th>                                
                                <th>@sortablelink('type_id')</th>
                                <th>@sortablelink('status')</th>
                                <th class="pull-right">Actions</th>
                            </tr>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member->name }}</td>                                    
                                    <td>
                                    @if($member->type)
                                    {{ $member->type->name }}
                                    @endif
                                    </td>
                                    <td>
                                        <input id="member_{{ $member->id }}" data-id="{{ $member->id }}" class="member_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $member->status == 'active' ? ' checked' : null }}>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-members')
                                            <a href="{{ url('dreamcms/members/'.$member->id.'/edit') }}"
                                               class="tool" data-toggle="tooltip" title="Edit"><i
                                                        class="fa fa-edit"></i></a>
                                            @endcan

                                            @can('delete-members')
                                            <a href="{{ url('dreamcms/members/'.$member->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                           @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $members->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $members->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.member_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "members/"+$(this).data('id')+"/change-member-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection