@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Members</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> Members</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Member</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/members/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $member->id }}">
                            <div class="box-body">
                                <h4>Member Details</h4>
                               
                                <div class="form-group {{ ($errors->has('firstName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">First Name *</label>

                                    <div class="col-sm-10">
                                        <input type="textbox" class="form-control" name="name"
                                               placeholder="Name"
                                               value="{{ old('name',$member->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                               
                                @php
                                    if(old('type_id')!=''){
                                        $type_id = old('type_id');
                                    }else{
                                        $type_id = $member->type_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('type_id')) ? ' has-error' : '' }}"
                                     id="type_selector">
                                    <label class="col-sm-2 control-label">Type</label>

                                    <div class="col-sm-10">
                                        <select name="type_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}"{{ ($type_id == $type->id) ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('short_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Short Description*</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="short_description"
                                                  placeholder="Short Description">{{ old('short_description',$member->short_description) }}</textarea>
                                        @if ($errors->has('short_description'))
                                            <small class="help-block">{{ $errors->first('short_description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body *</label>

                                    <div class="col-sm-10">
                                        <textarea id="body" name="body" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('body',$member->body) }}</textarea>
                                        @if ($errors->has('body'))
                                            <small class="help-block">{{ $errors->first('body') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                                               
                                                                
                                @php
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }else{
                                        $status = $member->status;
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                        </label>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/members') }}" class="btn btn-info pull-right"
                                       data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                       data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                       data-btn-cancel-label="No">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            CKEDITOR.replace('body');
            CKEDITOR.replace('short_description');

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection