@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Complaints</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fas fa-sticky-note"></i> Complaints</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/complaints') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <select name="type" class=" select2" style="width: 100%;">
                                    <option value="all" {{ $session['type'] == "" || $session['type']=="all" ? ' selected="selected"' : '' }}>
                                        All Types
                                    </option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}"{{ $session['type'] == $type->id ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/complaints/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                        </div>
                    </form>

                    <div class="pull-right box-tools">
                        @can('extract-complaints-report')
                        <a href="{{ url('dreamcms/complaints/extract-report') }}" type="button" class="btn bg-olive btn-sm"
                           data-widget="add">Extract Report
                            <i class="fa fa-plus"></i>
                        </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body">
                    @if(count($complaints))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('given_name')</th>
                                <th>@sortablelink('surname')</th>
                                <th>@sortablelink('typesort.name','Type')</th>
                                <th>@sortablelink('created_at', 'Submmited On')</th>
                                <th>@sortablelink('complaint_status_id', 'Status')</th>
                                <th>No. of Docs</th>
                                <th>Form</th>
                                <th class="pull-right">Actions</th>
                            </tr>
                            @foreach($complaints as $complaint)
                                <tr>
                                    <td>{{ $complaint->given_name }}</td>
                                    <td>{{ $complaint->surname }}</td>
                                    <td>
                                    @if($complaint->type)
                                    {{ $complaint->type->name }}
                                    @endif
                                    </td>
                                    <td>{{ $complaint->created_at->format('d/m/Y h:i:s A') }}</td>
                                    <td>{{ $complaint->status->name }}</td>
                                    <td>{{ count($complaint->documents) }}</td>
                                    <td><a href="{{ url('online-complaint/'.$complaint->complaint_receipt_number.'/pdf') }}" class="complaint-pdf-icon"><i class="fas fa-file-pdf"></i></a></td>
                                    <td>
                                        <div class="pull-right">
                                            <a href="{{ url('dreamcms/complaints/'.$complaint->id.'/view') }}" class="tool" data-toggle="tooltip" title="View"><i class="fa fa-search"></i></a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $complaints->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $complaints->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection