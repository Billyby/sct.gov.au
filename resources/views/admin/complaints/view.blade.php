@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Complaints</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/complaints') }}"><i class="fa fa-exclamation-circle"></i> Complaints</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <section class="invoice">
            <div class="row">
                <div class="col-xs-12">
                    <p class="lead">Personal Details of the Complainant</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Complaint Type:</th>
                                <td>{{ $complaint->type->name }}</td>
                            </tr>
                            <tr>
                                <th style="width:20%">Title:</th>
                                <td>{{ $complaint->title }}</td>
                            </tr>
                            <tr>
                                <th>Surname:</th>
                                <td>{{ $complaint->surname }}</td>
                            </tr>
                            <tr>
                                <th>Given Names:</th>
                                <td>{{ $complaint->given_name }}</td>
                            </tr>
                            <tr>
                                <th>Gender:</th>
                                <td>{{ $complaint->gender }}</td>
                            </tr>
                            <tr>
                                <th>Date of Birth:</th>
                                <td>{{ $complaint->dob }}</td>
                            </tr>
                            <tr>
                                <th>Postal Address:</th>
                                <td>{{ $complaint->address }}</td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>{{ $complaint->state->name }}</td>
                            </tr>
                            <tr>
                                <th>Post Code:</th>
                                <td>{{ $complaint->postcode }}</td>
                            </tr>
                            <tr>
                                <th>Country:</th>
                                <td>
                                @if(isset($complaint->country))
                                {{ $complaint->country->name }}
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Telephone:</th>
                                <td>{{ $complaint->phone }}</td>
                            </tr>
                            <tr>
                                <th>Mobile:</th>
                                <td>{{ $complaint->mobile }}</td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td>{{ $complaint->email }}</td>
                            </tr>
                            <tr>
                                <th>Facsimile:</th>
                                <td>{{ $complaint->fax }}</td>
                            </tr>
                            <tr>
                                <th>If you are complaining on behalf of another person please provide their name and date of birth:</th>
                                <td>{{ $complaint->on_behalf_name_dob }}</td>
                            </tr>
                            <tr>
                                <th>Relationship to you:</th>
                                <td>{{ $complaint->relationship }}</td>
                            </tr>
                            <tr>
                                <th>If your complaint is on behalf of a child, are you the legal guardian?</th>
                                <td>{{ $complaint->legal_guardian }}</td>
                            </tr>
                            <tr>
                                <th>If your complaint is on behalf of a person of legal age, do you hold a Power of Attorney?</th>
                                <td>{{ $complaint->poa }}</td>
                            </tr>
                        </table>
                    </div>

                    @if(isset($complaint->death))
                    <p class="lead">Personal Details of the Deceased Member</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:20%">Surname:</th>
                                <td>{{ $complaint->death->surname_deceased }}</td>
                            </tr>
                            <tr>
                                <th>Given Names:</th>
                                <td>{{ $complaint->death->given_name_deceased }}</td>
                            </tr>
                            <tr>
                                <th>Date of Birth:</th>
                                <td>{{ $complaint->death->date_deceased }}</td>
                            </tr>
                            <tr>
                                <th>Your relationship to the deceased:</th>
                                <td>{{ $complaint->death->relationship_deceased }}</td>
                            </tr>
                        </table>
                    </div>
                    @endif

                    <p class="lead">Details of Superannuation Provider</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:20%">Superannuation Provider Name:</th>
                                <td>{{ $complaint->super_provider_name }}</td>
                            </tr>
                            <tr>
                                <th>Member Name:</th>
                                <td>{{ $complaint->super_member_name }}</td>
                            </tr>
                            <tr>
                                <th>Member/Policy Number:</th>
                                <td>{{ $complaint->super_member_policy_number }}</td>
                            </tr>
                        </table>
                    </div>

                    <p class="lead">Representation</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:20%">Title:</th>
                                <td>{{ $complaint->rep_title }}</td>
                            </tr>
                            <tr>
                                <th>Surname:</th>
                                <td>{{ $complaint->rep_surname }}</td>
                            </tr>
                            <tr>
                                <th>Given Names:</th>
                                <td>{{ $complaint->rep_given_name }}</td>
                            </tr>
                            <tr>
                                <th>Postal Address:</th>
                                <td>{{ $complaint->rep_address }}</td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>{{ $complaint->repstate->name }}</td>
                            </tr>
                            <tr>
                                <th>Post Code:</th>
                                <td>{{ $complaint->rep_postcode }}</td>
                            </tr>
                            <tr>
                                <th>Country:</th>
                                <td>
                                @if(isset($complaint->repcountry))
                                {{ $complaint->repcountry->name }}
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Telephone:</th>
                                <td>{{ $complaint->rep_phone }}</td>
                            </tr>
                            <tr>
                                <th>Mobile:</th>
                                <td>{{ $complaint->rep_mobile }}</td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td>{{ $complaint->rep_email }}</td>
                            </tr>
                            <tr>
                                <th>Facsimile:</th>
                                <td>{{ $complaint->rep_fax }}</td>
                            </tr>
                            <tr>
                                <th>Relationship to you:</th>
                                <td>{{ $complaint->rep_relationship }}</td>
                            </tr>
                            <tr>
                                <th>Reasons for wanting to be represented:</th>
                                <td>{{ $complaint->rep_reasons }}</td>
                            </tr>
                        </table>
                    </div>

                    @if(isset($complaint->disability))
                    <p class="lead">Disability Claim and your Employment</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:20%">What type of benefit is in dispute?</th>
                                <td>{{ $complaint->disability->type->name }}</td>
                            </tr>
                            <tr>
                                <th>Date you were last physically at work?</th>
                                <td>{{ $complaint->disability->dis_last_attended_date }}</td>
                            </tr>
                            <tr>
                                <th>Date you permanently ceased employment?</th>
                                <td>{{ $complaint->disability->dis_ceased_work_date }}</td>
                            </tr>
                            <tr>
                                <th>Reason for employment ceased:</th>
                                <td>{{ $complaint->disability->dis_ceased_reasons }}</td>
                            </tr>
                            <tr>
                                <th>Did you permanently cease employment because of the physical or mental condition which caused you to make a disability claim?</th>
                                <td>{{ $complaint->disability->dis_physical_mental }}</td>
                            </tr>
                            <tr>
                                <th>Did your Employer provide you with written notification that your employment had ceased?</th>
                                <td>{{ $complaint->disability->dis_employer_notice }}</td>
                            </tr>
                            <tr>
                                <th>What date did you lodge your Disability claim with the Fund?</th>
                                <td>{{ $complaint->disability->dis_claim_lodge_date }}</td>
                            </tr>
                            <tr>
                                <th>Have you been in receipt of any workers' compensation payments that relate to the physical or mental condition with which the Disability claim may relate?</th>
                                <td>{{ $complaint->disability->dis_worker_compensation }}</td>
                            </tr>
                            <tr>
                                <th>If yes, what date did your payments commence?</th>
                                <td>{{ $complaint->disability->dis_payment_start_date }}</td>
                            </tr>
                            <tr>
                                <th>What date did the payments cease?</th>
                                <td>{{ $complaint->disability->dis_payment_ceased_date }}</td>
                            </tr>
                        </table>
                    </div>
                    @endif

                    <p class="lead">Details of your Complaint</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:20%">Outline what has happened and state what particular decisions or actions of the superannuation provider you are complaining about:</th>
                                <td>{{ $complaint->complaint_details }}</td>
                            </tr>
                            <tr>
                                <th>State why you consider the superannuation provider's decisions or actions are unfair or unreasonable:</th>
                                <td>{{ $complaint->complaint_unfair_details }}</td>
                            </tr>
                            @if($complaint->complaint_type_id!=3)
                            <tr>
                                <th>What is the amount of the benefit in dispute?</th>
                                <td>
                                @if(isset($complaint->complaint_amount_dispute))
                                ${{ $complaint->complaint_amount_dispute }}
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <th>What outcomes are you seeking to resolve your complaint?</th>
                                <td>{{ $complaint->complaint_outcomes }}</td>
                            </tr>
                            @endif
                        </table>
                    </div>

                    @if(isset($complaint->other))
                    <p class="lead">Details of your complaint – reliance on information or representation</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:20%">What was the information or representation that you relied on?</th>
                                <td>{{ $complaint->other->all_info_rep_relied }}</td>
                            </tr>
                            <tr>
                                <th>What did the information or representation lead you to expect would happen?</th>
                                <td>{{ $complaint->other->all_info_rep_expectation }}</td>
                            </tr>
                            <tr>
                                <th>How did you rely on the information or representation?</th>
                                <td>{{ $complaint->other->all_info_rep_how }}</td>
                            </tr>
                            <tr>
                                <th>What would you have done differently if you had known the correct or full information or the representation had not been made?</th>
                                <td>{{ $complaint->other->all_info_rep_action }}</td>
                            </tr>
                            <tr>
                                <th>What is the amount of the benefit in dispute?</th>
                                <td>
                                @if(isset($complaint->complaint_amount_dispute))
                                ${{ $complaint->complaint_amount_dispute }}
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <th>What outcomes are you seeking to resolve your complaint?</th>
                                <td>{{ $complaint->complaint_outcomes }}</td>
                            </tr>
                        </table>
                    </div>
                    @endif

                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="lead">Files</p>
                    @if(count($complaint->documents)>0)
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>File Name</th>
                                <th>Title</th>
                                <th>Description</th>
                            </tr>
                            @foreach($complaint->documents as $document)
                            <tr>
                                <td><a href="{{ url('dreamcms/complaints/'.$document->id.'/download') }}" target="_blank">{{ $document->complaint_document }}</a></td>
                                <td>{{ $document->document_title }}</td>
                                <td>{{ $document->document_description }}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @else
                        No file
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <p class="lead">SCT Office Use Only</p>

                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/complaints/update') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $complaint->id }}">

                        <div class="form-group {{ ($errors->has('notes')) ? ' has-error' : '' }}">
                            <label class="col-sm-12">Notes :</label>

                            <div class="col-sm-9">
                                <textarea class="form-control" rows="3" name="notes" placeholder="Notes">{{ $complaint->notes }}</textarea>
                                @if ($errors->has('notes'))
                                    <small class="help-block">{{ $errors->first('notes') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ ($errors->has('last_reviewed_date')) ? ' has-error' : '' }}">
                            <label class="col-sm-2">Last Reviewed Date</label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="last_reviewed_date" type="text" class="form-control pull-right datepicker" value="{{ $complaint->complaint_reviewed_date }}">
                                </div>
                                @if ($errors->has('last_reviewed_date'))
                                    <small class="help-block">{{ $errors->first('last_reviewed_date') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ ($errors->has('actioned')) ? ' has-error' : '' }}">
                            <label class="col-sm-2">Mark as Actioned?</label>
                            <div class="col-sm-7">
                                <input type="checkbox" class="minimal" name="actioned" {!! ($complaint->actioned==1) ? ' checked' : null !!}>
                            </div>
                        </div>

                        <div class="form-group{{ ($errors->has('actioned')) ? ' has-error' : '' }}">
                            <div class="col-sm-9">
                                <a href="{{ url('dreamcms/news') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save & Close</button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </section>
        <div class="clearfix"></div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('notes');

            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    </script>
@endsection