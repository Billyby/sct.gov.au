@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="container">
        <h1> <i class="fas fa-exclamation-triangle"></i> Page not found!</h1>
    </div>

@endsection