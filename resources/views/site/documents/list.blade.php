@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-documents')        
        
        <div class="col-sm-8 blog-main">
          @if(isset($category)) 
          <h1>{{ $category->name }}</h1>
          
          @if($category->description != "")
             {!! $category->description !!}
          @endif
          @endif
                 
		  @if(isset($items))        
			 @foreach ($items as $item)	
				<div class="blog-post">   
	           
		           <div class='document-wrapper'>
				      <a href='{{ url('') }}{{ (strrpos($item->fileName, "/") === false ? "/media/documents/" . str_replace('public/','',$item->fileName) : str_replace('public/','',$item->fileName))}}' target='_blank'>{{$item->title}} <i class="fa fa-file-pdf"></i></a>
				   </div>
				                                     					
				</div><!-- /.blog-post -->         
		 	 @endforeach 
		  @endif			     
                               
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
