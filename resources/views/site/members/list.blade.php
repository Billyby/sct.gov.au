<div class="blog-post faqs">          
@if (isset($members))        
	@foreach ($members as $item)					
		<h4><a class="accordion-toggle" data-toggle="collapse" href="#faqitem{{ $loop->iteration }}">{{$item->name}}</a></h4>

		<div id="faqitem{{ $loop->iteration }}" class="panel-collapse collapse in"> {!! $item->body !!} </div>                                   					
	@endforeach 
@endif			     

</div><!-- /.blog-post -->         
       