@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

        <div class="container">

            <h2>Site search</h2>

            <form method="post" action="{{ url('results') }}">
                {{ csrf_field() }}
                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="col-sm-8"><input type="text" name="query" class="form-control" placeholder="Search for..." value="{{ $query }}"></div>
                        <div class="col-sm-2"><button type="submit" class="btn btn-secondary mb-2">Search</button></div>
                    </div>
                </div>
            </form>

            @foreach($results as $result)
            @php
            if($result['type']=='page'){
                $url = 'pages/'.$result['item']->category->slug.'/'.$result['item']->slug;
                $link = $result['item']->title;
                $description = $result['item']->body_clean;
            }

            if($result['type']=='news'){
                $url = 'news/'.$result['item']->category->slug.'/'.$result['item']->slug;
                $link = $result['item']->title;
                $description = $result['item']->body_clean;
            }

            if($result['type']=='document'){
                $url = 'documents/'.$result['item']->category->slug;
                $link = $result['item']->title;
                $description = $result['item']->description_clean;
            }

            if($result['type']=='faq'){
                $url = 'faqs/'.$result['item']->category->slug;
                $link = $result['item']->title;
                $description = $result['item']->description_clean;
            }
            @endphp
            <div class="row result">
                <div class="col-12"><a href="{{ url($url) }}" class="result-link">{{ $link }}</a></div>
                <div class="col-12">{{ $description }}</div>
                <div class="col-12"><hr /></div>
            </div>
            @endforeach
            {{ $results->links() }}
        </div>

@endsection

@section('scripts')
    <script src="{{ asset('components/mark.js/dist/jquery.mark.js') }}"></script>
@endsection

@section('sec-inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".result").mark('{{ addslashes($query) }}');
        });
    </script>
@endsection