@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="container">
        <h5> <i class="fas fa-exclamation-triangle"></i>Your email address has been successfully confirmed.</h5>
    </div>

@endsection