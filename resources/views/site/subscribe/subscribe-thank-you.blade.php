@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="container">
        <h5> <i class="fas fa-exclamation-triangle"></i>Thanks for subscribing to the SCT email list. To complete your subscription, please click the link in the confirmation email.</h5>
    </div>

@endsection