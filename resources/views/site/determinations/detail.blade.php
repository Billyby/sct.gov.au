@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="container">

        <h2>Determination Details</h2>

        <div class="row">
            <div class="col-4">
                <div class="col-12"><strong>Determination Number:</strong> {{ $determination->determination_number }}</div>
                <div class="col-12"><strong>File Number:</strong> {{ $determination->file_number }}</div>
                <div class="col-12"><strong>Determination Date:</strong> {{ date('d/m/Y', $determination->determination_date) }}</div>
            </div>
            <div class="col-8">
                <a href="{{ url('search-determinations/'.$determination->id.'/download') }}" target="_blank">
                    <i class="fas fa-download"></i>
                    Full Determination (PDF)
                </a>
            </div>
            <div class="col-12"><hr/></div>
        </div>

        <div class="row">
            <div class="col-12">{!! $determination->description_detail !!}</div>
        </div>

    </div>

@endsection