@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

        <div class="container">

            <h2>Search determinations</h2>

            <form method="post" action="{{ url('search-determinations/results') }}">
                {{ csrf_field() }}

                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="col-sm-8"><input type="text" name="query" class="form-control" placeholder="Search for..." value="{{ $query }}"></div>
                        <div class="col-sm-2">
                            <select name="year" class="form-control">
                                <option value="All"{{ ($year == 'All' || $year == NULL) ? ' selected="selected"' : '' }}>All Years</option>
                                @for($i=1995; $i<=date('Y'); $i++)
                                    <option value="{{ $i }}"{{ ($year == $i) ? ' selected="selected"' : '' }}>{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-2"><button type="submit" class="btn btn-secondary mb-2">Search</button></div>
                    </div>
                </div>

            </form>

            @if(count($results)>0)
                <div class="row">
                    <div class="col-12">

                        @if($year=='All')

                            @if(Request::get('sort_by')=='date')
                                <a href="{{ Request::url().'?query='.$query.'&year='.$year }}">Sort by revelance</a> /
                                Sort by date
                            @else
                                Sort by revelance /
                                <a href="{{ Request::url().'?query='.$query.'&year='.$year.'&sort_by=date' }}">Sort by date</a>
                            @endif

                        @endif

                    </div>
                </div><br />
                @foreach($results as $determination)
                <div class="row result">
                    <div class="col-12"><a href="{{ url('search-determinations/'.$determination->id.'/detail') }}" class="result-link">{{ $determination->determination_number }}</a></div>
                    <div class="col-12"><strong>Year of determination:</strong> {{ date('Y',$determination->determination_date) }}</div>
                    <div class="col-12">{{ $determination->description_clean }}</div>
                    <div class="col-12"><hr /></div>
                </div>
                @endforeach

                @if(Request::get('sort_by')=='date')
                    {{ $results->appends(['query' => $query,'year' => $year, 'sort_by' => 'date'])->links() }}
                @else
                    {{ $results->appends(['query' => $query,'year' => $year])->links() }}
                @endif

            @else
                No results were found
            @endif
        </div>

@endsection

@section('scripts')
    <script src="{{ asset('components/mark.js/dist/jquery.mark.js') }}"></script>
@endsection

@section('sec-inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".result").mark('{{ addslashes($query) }}');
        });
    </script>
@endsection