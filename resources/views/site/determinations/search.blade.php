@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

        <div class="container">

            <h2>Search determinations</h2>

            <form method="post" action="{{ url('search-determinations/results') }}">
                {{ csrf_field() }}

                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="col-sm-8"><input type="text" name="query" class="form-control" placeholder="Search for..."></div>
                        <div class="col-sm-2">
                            <select name="year" class="form-control">
                                <option value="All">All Years</option>
                                @for($i=1995; $i<=date('Y'); $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-2"><button type="submit" class="btn btn-secondary mb-2">Search</button></div>
                    </div>
                </div>

            </form>

        </div>

@endsection
