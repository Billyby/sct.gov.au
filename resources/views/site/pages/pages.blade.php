<?php 
   // Set Meta Tags
   $meta_title_inner = $page->meta_title; 
   $meta_keywords_inner = $page->meta_keywords; 
   $meta_description_inner = $page->meta_description;  
$company_name = "";
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @if ($category[0]->slug != "news-and-publications")
           @include('site/partials/sidebar-pages')        
        @else
           @include('site/partials/sidebar-news')        
        @endif
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            @if (isset($page))
            <h1 class="blog-post-title">{{$page->title}}</h1>
            {!! $page->body !!}
            @endif
                        
            @if ($category[0]->slug == 'about-us' && $page->slug == 'who-we-are') 
                <!-- Members -->
                @include('site/members/list')
            @endif
            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
