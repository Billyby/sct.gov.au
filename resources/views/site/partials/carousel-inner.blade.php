@section('inline-scripts')
<script type="text/javascript"> 
var images = [
    <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
	
    "{{ $image->location }}",
	
	@endforeach	
    ];

// The index variable will keep track of which image is currently showing
var index = 0,oldIndex;

$(document).ready(function() {
    
    // Call backstretch for the first time,
    // In this case, I"m settings speed of 500ms for a fadeIn effect between images.
    index = Math.floor((Math.random()*images.length));
    $.backstretch(images[index], {
        transitionDuration: 1000
    });

    // Set an interval that increments the index and sets the new image
    // Note: The fadeIn speed set above will be inherited
    //
    
    setInterval(function() {
       oldIndex = index;
        while (oldIndex == index) {
            index = Math.floor((Math.random()*images.length));
        }
        $.backstretch(images[index]);
    }, 10000);

    // A little script for preloading all of the images
    // It"s not necessary, but generally a good idea
    $(images).each(function() {
        $("<images/>")[0].src = this;
    });
	
	// Accessibility - Text Resizer
	$('.fontsizer').jfontsizer({
		applyTo: '.blog-post p, .blog-post li, .blog-post a, .blog-post td',
		changesmall: '2',
		changelarge: '3',
		expire: 30
	});
    
});
</script>

@endsection


@include('site/partials/header')