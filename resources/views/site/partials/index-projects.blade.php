@if(isset($home_projects))
	 <div class="home-projects">
	   <div class="container">
		  <div class="row">         
			 @foreach($home_projects as $item)       	 
				  <div class="col-lg-4">
			           <h2>{{ $item->title }}</h2>
			           {!! $item->short_description !!}
			           
				       @if($item->thumbnail != "")
				          <div class="home-projects-img">
					         <img class="rounded-circle" src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{ $item->title }}" width="140" height="140" />
					      </div>
					   @endif					   					 
					   					   
					   <p><a class="btn btn-secondary" href="{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}" role="button">View details &raquo;</a></p>
				  </div><!-- /.col-lg-4 -->
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif