@if(isset($mode) && $mode == 'preview')        
	<style>
		.fixed-top {position: relative;}
		body {padding-top: 52px;}
	</style>
	<div class='preview'>	   
		<a href="{{ url('dreamcms/dashboard') }}" class="logo">        
			<span class="logo-lg"><img src="{{ asset('images/admin/logo.png') }}" alt="Echo3" title="Echo3" class='navbar-logo'> Dream<b>CMS</b></span>
		</a>	
		<span class="spacer">|</span> Preview Page</span>		
		
		<div class='preview-return'>
			<a href="javascript: window.close();">
				<i class="fa fa-times"></i> CLOSE
			</a>
		</div>
		
		<div class='preview-return'>
			<a href="javascript: location.reload();">
				<i class="fa fa-sync"></i> REFRESH
			</a>
		</div>
	</div>
@endif