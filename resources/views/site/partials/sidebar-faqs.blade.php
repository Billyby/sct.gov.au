<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Frequently asked questions</h4>
	<ol class="navsidebar list-unstyled">             
	  @foreach ($side_nav as $item)	 
		 <li class='{{ ($item->id == $items[0]->category_id ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/faqs/{{ $item->slug }}">{{ $item->name }}</a></li>		
	  @endforeach              
	</ol>
  </div>          
</div>