@include('site/partials/header')

<div id="myCarousel" class="carousel sslide" data-ride="ccarousel">
 <ol class="carousel-indicators">
    <?php $counter = 0; ?>
    @foreach($images as $image)        
       <li data-target="#myCarousel" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>       
       <?php $counter++;  ?>
    @endforeach 
  </ol>
  <div class="carousel-inner">
    
        <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
               
			<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
			  <img class="slide" src="{{ $image->location }}" alt="{{ $image->title }}">
				<div class="carousel-caption text-left">
				    <div class="carousel-link-container">
                        @if ( $image->title != "") <h1><a class="carousel-link" href="{{ $image->url }}"> {{ $image->title }}</a></h1> @endif
                        @if ( $image->description != "") <p class="carousel-description">{{ $image->description }}</p> @endif
                    </div>
				</div>
			</div>
        @endforeach
   
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>