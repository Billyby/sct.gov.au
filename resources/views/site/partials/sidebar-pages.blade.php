<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>{{ $category[0]->name }}</h4>
	<ol class="navsidebar list-unstyled">             
	  @foreach ($side_nav as $item)
		 <li class='{{ ($item->slug == $page->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item->slug }}">{{ $item->title }}</a>

			 <ol class="navsidebar navsidebar-sub list-unstyled navsidebar-sub-sub">  
				@foreach ($item->nav_sub as $item_sub)            
				 <li class='{{ ($item_sub->slug == $page->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item_sub->slug }}">{{ $item_sub->title }}</a></li>             
				@endforeach						
			 </ol>	
		 
		 </li>	 		 
	  @endforeach
	  
	  @if ($category[0]->slug == "determinations")
	    <li class=''><a class="navsidebar" href="{{ url('') }}/search-determinations">Search Determinations</a></li>
	  @endif
	</ol>
  </div>          
</div>