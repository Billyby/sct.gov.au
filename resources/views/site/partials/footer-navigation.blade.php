<div class="container footer-container">
<div class='footerNav'>
<hr>
 <div class='footerNavWrapper'>	               
	  <div class="row">         
		 <div class="col">
			<img class="footer-logo" src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}">
		 </div>	

		 <div class="col">
			 <a href='{{ url('') }}/pages/about-my-complaint/time-limits'>Time limits</a><br>
			 <a href='{{ url('') }}/pages/determinations/overview'>Determinations</a><br>
			 <a href='{{ url('') }}/pages/about-my-complaint/case-study'>Case study</a><br>
		 </div>	

		 <div class="col">
			 <a href='{{ url('') }}/faqs/glossary'>Glossary of terms</a><br>
			 <a href='{{ url('') }}/pages/contact-us/other-bodies'>Useful links</a><br>
			 <a href='{{ url('') }}/pages/legislation/freedom-of-information-foi'>Freedom of Information (FOI)</a><br>   
		 </div>		
     
     	 <div class="col">
			 <a href='{{ url('') }}/pages/other/privacy-policy'>Privacy Policy</a><br>
			 <a href='{{ url('') }}/pages/other/disclaimer'>Disclaimer</a><br>
			 <a href='{{ url('') }}/pages/contact-us'>Contact Us</a><br>
		 </div>		    	     
      </div>
  </div>
</div>
</div>  