<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark btco-hover-menu">	      
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
				<a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span class="sr-only">(current)</span></a>
			</li>
			@if(count($navigation))
               @foreach($navigation as $nav_item)                 
                 @if ($nav_item["slug"] != "news-and-publications")                    						
					<li class="nav-item dropdown">
						<a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ ($nav_item["slug"]=='other') ? $nav_item["slug"]."/privacy-policy" : $nav_item["slug"] }}" id="navbarDropdownMenuLink">
							{{ ($nav_item["display_name"] != "Other" ? $nav_item["display_name"] : "Privacy Policy") }}
						</a>
						
						@if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 0 && $nav_item["slug"] != "other")
							<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							    @if ($nav_item["slug"] == "news")
								   <li><a class="dropdown-item" href="{{ url('') }}/pages/news-and-publications/overview" id="navbarDropdownMenuLink">Overview</a></li>							
								@endif
						    
							    @foreach($nav_item["nav_sub"] as $nav_sub)	
								
									<li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>
									
									    @if(isset($nav_item["nav_sub_sub"]))
											<ul class="dropdown-menu">
												@foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)	
												   @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])														  
													  <li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub_sub["slug"] }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a></li>
												   @endif
												@endforeach											
											</ul>
										@endif
										
									</li>
								@endforeach
								
								 @if ($nav_item["slug"] == "news")  
								     @foreach($navigation_documents as $nav_sub)	
								        <li><a class="dropdown-item" href="{{ url('') }}/documents/{{ $nav_sub["slug"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>
									 @endforeach
								 @endif
								 
								 @if ($nav_item["slug"] == "determinations")
	                                 <li class=''><a class="dropdown-item" href="{{ url('') }}/search-determinations">Search determinations</a></li>
	                             @endif	
								 
							</ul>
						@endif
						
					</li>
				  @endif				  			  				  				  
			   @endforeach                          
            @endif
                                    			
		</ul>	  
	</div>
</nav>