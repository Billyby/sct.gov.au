<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>News & publications</h4>
	<ol class="navsidebar list-unstyled">
         <li class=''><a class="navsidebar" href="{{ url('') }}/pages/news-and-publications/overview">Overview</a></li>     
         
	  @foreach ($side_nav as $item)
		 <li class=''><a class="navsidebar" href="{{ url('') }}/news/{{ $item->slug }}">{{ $item->name }}</a></li>               
	  @endforeach 	
	  
	  @foreach ($side_nav_documents as $item)
		 <li class=''><a class="navsidebar" href="{{ url('') }}/documents/{{ $item->slug }}">{{ $item->name }}</a></li>               
	  @endforeach 	                                      
	</ol>
	
	<div class='btn-back'>
       <a class='btn-back' href='{{ url('') }}/news/archive'>ARCHIVED NEWS <i class='fa fa-chevron-right'></i></a>	
	</div>
	
	
	@include('site/partials/sidebar-news-newsletter')        
  </div>    
</div>