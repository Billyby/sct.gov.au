<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>News & publications</h4>
	<ol class="navsidebar list-unstyled"> 
      <li class=''><a class="navsidebar" href="{{ url('') }}/pages/news-and-publications/overview">Overview</a></li> 
      
          
      @if (isset($side_nav))           
		  @foreach ($side_nav_news as $item)	 
			 <li class='{{ ( sizeof($items) > 0 && $items[0]->category_id == $item->id  ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/news/{{ $item->slug }}">{{ $item->name }}</a></li>		
		  @endforeach
		  
		  @foreach ($side_nav as $item)
		    <li class=''><a class="navsidebar" href="{{ url('') }}/documents/{{ $item->slug }}">{{ $item->name }}</a></li>               
	      @endforeach 	            
	  @endif  
	</ol>
  </div>          
</div>