<div class='search'>
 <div class='searchWrapper'>    
  <h2>Search the determinations database</h2>
  
  <form method="post" action="{{ url('search-determinations/results') }}">
	  {{ csrf_field() }}
 	  <div class="search-row">
  	     <input type="textbox" class="search-box" name="query" placeholder="Search for...">
  	     <input type="submit" value="Go" class="btn-search f-btn-search">
	  </div>

 	  <div class="search-row">
  	     <a href="{{ url('search-determinations') }}">Advanced search ></a>
	  </div>
	  
	  <div class="search-row">
  	     <a href='{{ url('') }}/pages/legislation/information-publication-scheme'><img src="{{ url('') }}/images/site/logo-ips.png" title="Information Pulication Scheme" alt="Information Pulication Scheme"></a>
  	     <a href='{{ url('') }}/pages/legislation/tribunal-disclosure-log'><img src="{{ url('') }}/images/site/logo-foidl.png" title="FOI Disclosure Log" alt="FOI Disclosure Log"></a>	
      </div>		          
  </form>
 </div> 
</div>