<div class="print-page">
   <a href="javascript:" onclick="print()" alt="Print Page" title="Print Page" ><img src="{{ url('') }}/images/site/icon-printer.png" alt="print page" /></a>
</div>

<div class="fontsizer"></div>

@section('inline-scripts')
<!-- Accessibility - Text Resizer -->     
<script type="text/javascript">
	$(document).ready(function(){		
		$('.fontsizer').jfontsizer({
			applyTo: '#intro p, .home-buttons h2',
			changesmall: '2',
			changelarge: '3',
			expire: 30
		});
	});
</script>
@endsection
