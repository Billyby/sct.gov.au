 <div class="home-buttons">   
	  <div class="row">         		    	 
		  <div onclick="location.href = '{{ url('') }}/pages/make-a-complaint/what-does-the-tribunal-cover';" class="col-lg-4 home-button1">
		      <img src="{{ url('') }}/images/site/icon1.png" title="How can the Tribunal help me?" alt="How can the Tribunal help me?">
			  <h2>How can the</h2>
			  <h2>Tribunal help me?</h2>
		  </div><!-- /.col-lg-4 -->	
		  
		  	<div onclick="location.href = '{{ url('') }}/pages/make-a-complaint/lodging-a-complaint';" class="col-lg-4 home-button2">
			   <img src="{{ url('') }}/images/site/icon2.png" title="Online complaint form" alt="Online complaint form">	
			   <h2>Online complaint</h2> 
			   <h2>form</h2>				  
		  </div><!-- /.col-lg-4 -->	
		  
		  	<div onclick="location.href = '{{ url('') }}/pages/make-a-complaint/online-complaint';" class="col-lg-4 home-button3">
			   <img src="{{ url('') }}/images/site/icon3.png" title="How do I make a complaint?" alt="How do I make a complaint?">	
			   <h2>How do I make</h2> 
			   <h2>a complaint?</h2>					  
		  </div><!-- /.col-lg-4 -->		
		</div>   
</div>
