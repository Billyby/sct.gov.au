<div id="header-logo" class="header-logo">
	<div class='header-logo-mobile'>
		<a href="{{ url('') }}" title="{{ $company_name }}"><img id='header-logo-img' src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
	</div>
</div>

<div class="header-search">
	<form class="form-inline mt-2 mt-md-0" method="post" action="{{ url('results') }}">
		{{ csrf_field() }}
	   <input class="form-control mr-sm-2 search-txt" type="text" placeholder="Site search" aria-label="Search" name="query" id="txtSearch" >
	   <button class="btn btn-search" type="submit"><i class='fa fa-search'></i></button>
	</form>
</div>

<div class="header-resizer">
  @include('site/partials/helper-resizer')
</div>