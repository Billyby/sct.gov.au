@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-faqs')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post faqs">  
			<h1>Frequently asked questions</h1>

            <p>You may have many questions concerning your complaint and the process undertaken to have it resolved. As many people have similar questions, it is advisable to check the FAQs below. If you find a similar question, click on the link to view the answer.</p>        
            @if (isset($items))        
				@foreach ($items as $item)					
					<h4><a class="accordion-toggle" data-toggle="collapse" href="#faqitem{{ $loop->iteration }}">{{$item->title}}</a></h4>
					
					<div id="faqitem{{ $loop->iteration }}" class="panel-collapse collapse in"> {!! $item->description !!} </div>                                   					
				@endforeach 
            @endif			     
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
