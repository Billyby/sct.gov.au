@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="container">

        <h2>Thank you</h2>
        <p>Your online complaint form has now been submitted to the Tribunal.<br />
            To view and save your online complaint form in PDF format, <a href="{{ url('online-complaint/'.$complaint->complaint_receipt_number.'/pdf') }}">click here</a></p>
    </div>
@endsection