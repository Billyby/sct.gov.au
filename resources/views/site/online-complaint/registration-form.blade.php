@extends('site.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/online-complaint-steps.css') }}">
@endsection
@section('content')

    @include('site/partials/carousel-inner')

    <div class="container online-complaint">

        <h2>Online Complaint Registration Form</h2>
        <div><strong>Please complete the Registration of Complaint form below.</strong></div>
        <div>* Denotes the mandatory field</div>
        <br/>
        <div>Have you made a complaint to your superannuation provider?
            <strong>{{ old('fc_officer', $request->fc_officer) }}</strong></div>
        <div>Has the superannuation provider responded to your complaint?
            <strong>{{ old('fc_response', $request->fc_response) }}</strong>
        </div>
        <div>Has 90 days passed since the superannuation provider received your complaint?
            <strong>{{ old('expiry', $request->expiry) }}</strong></div>
        <div>Complaint Type : <strong>{{ old('c_type', $request->c_type) }}</strong></div>
        <hr/>

        <form id="complaint-form" enctype="multipart/form-data" method="POST" action="{{ url('online-complaint/file-upload') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="fc_officer" value="{{ old('fc_officer', $request->fc_officer) }}">
            <input type="hidden" name="fc_response" value="{{  old('fc_response', $request->fc_response) }}">
            <input type="hidden" name="expiry" value="{{  old('expiry', $request->expiry) }}">
            <input type="hidden" name="c_type" value="{{  old('c_type', $request->c_type) }}">

            <h5>Personal details of the Complainant</h5>
            <section>
                <div class="form-row">
                    <label class="col-12 col-sm-2">Title</label>
                    <div class="col-12 col-sm-10 ">
                        <select name="title" class="custom-select">
                            <option value="N/A"{{ (old('title') == 'N/A') ? ' selected' : '' }}>Please Select</option>
                            <option value="Mr"{{ (old('title') == 'Mr') ? ' selected' : '' }}>Mr</option>
                            <option value="Mrs"{{ (old('title') == 'Mrs') ? ' selected' : '' }}>Mrs</option>
                            <option value="Ms"{{ (old('title') == 'Ms') ? ' selected' : '' }}>Ms</option>
                            <option value="Miss"{{ (old('title') == 'Miss') ? ' selected' : '' }}>Miss</option>
                            <option value="Dr"{{ (old('title') == 'Dr') ? ' selected' : '' }}>Dr</option>
                            <option value="Professor"{{ (old('title') == 'Professor') ? ' selected' : '' }}>Professor
                            </option>
                            <option value="Other"{{ (old('title') == 'Other') ? ' selected' : '' }}>Other</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Surname *</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control required" name="surname" value="{{ old('surname') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Given Names *</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control required" name="given_names"
                               value="{{ old('given_names') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Gender</label>
                    <div class="col-12 col-sm-10 ">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="gender"
                                       value="1"{{ (old('gender') == '1') ? ' checked' : '' }}>
                                Male
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="gender"
                                       value="2"{{ (old('gender') == '2') ? ' checked' : '' }}>
                                Female
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Date of Birth</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control datepicker" name="dob" value="{{ old('dob') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Postal Address</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="postal_address"
                               value="{{ old('postal_address') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">State</label>
                    <div class="col-12 col-sm-10 ">
                        <select name="state" class="custom-select">
                            <option value="10">Please Select</option>
                            @foreach($states as $state)
                                <option value="{{ $state->id }}"{{ (old('state') == $state->id) ? ' selected' : '' }}>{{ $state->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Post Code</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="postcode" value="{{ old('postcode') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Country</label>
                    <div class="col-12 col-sm-10 ">
                        <select name="country" class="custom-select">
                            <option value="">Please Select</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}"{{ (old('country') == $country->id) ? ' selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Telephone</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Mobile</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Email *</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control required email" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Facsimile</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="fax" value="{{ old('fax') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-md-6 col-form-label">If you are complaining on behalf of another person
                        please provide their name and date of birth (e.g. John Doe, 1 Jan 1980)</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="on_behalf_name_dob"
                               value="{{ old('on_behalf_name_dob') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-md-6 col-form-label">Relationship to you (e.g child)</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="relationship" value="{{ old('relationship') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-md-8 col-lg-6 col-form-label">If your complaint is on behalf of a child, are you
                        the
                        legal guardian?</label>
                    <div class="col-md-4 col-lg-6">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="legal_guardian"
                                       value="1"{{ (old('legal_guardian') == '1') ? ' checked' : '' }}>
                                Yes
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="legal_guardian"
                                       value="2"{{ (old('legal_guardian') == '2') ? ' checked' : '' }}>
                                No
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-md-8 col-lg-6 col-form-label">If your complaint is on behalf of a person of legal
                        age, do you hold a Power of Attorney?</label>
                    <div class="col-md-4 col-lg-6">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="power_attorney"
                                       value="1"{{ (old('power_attorney') == '1') ? ' checked' : '' }}>
                                Yes
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="power_attorney"
                                       value="2"{{ (old('power_attorney') == '2') ? ' checked' : '' }}>
                                No
                            </label>
                        </div>
                    </div>
                </div>
            </section>

            @if(old('c_type', $request->c_type)=='Death')
                <h5>Personal Details of the Deceased Member</h5>
                <section>
                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">Surname</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control" name="surname_deceased"
                                   value="{{ old('surname_deceased') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">Given Names</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control" name="name_deceased"
                                   value="{{ old('name_deceased') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">Date of Birth</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control datepicker" name="date_deceased"
                                   value="{{ old('date_deceased') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">Your relationship to the deceased (e.g. spouse,
                            child,
                            parent, executor).</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control" name="relationship_deceased"
                                   value="{{ old('relationship_deceased') }}">
                        </div>
                    </div>
                </section>
            @endif

            <h5>Details of Superannuation Provider</h5>
            <section>
                <div class="form-row">
                    <label class="col-12 col-sm-4 col-form-label">Superannuation Provider Name *</label>
                    <div class="col-12 col-sm-8">
                        <input type="text" class="form-control required" name="super_name"
                               value="{{ old('super_name') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-4 col-form-label">Member Name (if not the complainant)</label>
                    <div class="col-12 col-sm-8">
                        <input type="text" class="form-control" name="member_name" value="{{ old('member_name') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-4 col-form-label">Member/Policy Number</label>
                    <div class="col-12 col-sm-8">
                        <input type="text" class="form-control" name="super_policy" value="{{ old('super_policy') }}">
                    </div>
                </div>
            </section>

            <h5>Representation</h5>
            <section>
                <p>The Tribunal was established to be informal and economical and complainants are expected to represent
                    themselves. However, the Tribunal has the discretion under section23 of the Superannuation
                    (Resolution
                    of Complaints) Act 1993 to allow a party to a complaint to be represented by an agent if the
                    Tribunal
                    considers it to be necessary in all the circumstances.</p>
                <p>If you wish to be represented by an agent, you must provide reasons as to why it is necessary that
                    you be
                    represented. Upon receipt of your reasons, the Tribunal will make a decision in relation to
                    representation and advise you accordingly.</p>
                <p>Please note that the Tribunal is not empowered to award costs in relation to any fees arising as a
                    result
                    of representation. In other words, YOU will have to bear the costs of your representation, if
                    any.</p>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Title</label>
                    <div class="col-12 col-sm-10 ">
                        <select name="rep_title" class="custom-select">
                            <option value="N/A" {{ (old('rep_title') == 'N/A' || old('rep_title') == '') ? ' selected' : '' }}>
                                Please Select
                            </option>
                            <option value="Mr"{{ (old('rep_title') == 'Mr') ? ' selected' : '' }}>Mr</option>
                            <option value="Mrs"{{ (old('rep_title') == 'Mrs') ? ' selected' : '' }}>Mrs</option>
                            <option value="Ms"{{ (old('rep_title') == 'Ms') ? ' selected' : '' }}>Ms</option>
                            <option value="Miss"{{ (old('rep_title') == 'Miss') ? ' selected' : '' }}>Miss</option>
                            <option value="Dr"{{ (old('rep_title') == 'Dr') ? ' selected' : '' }}>Dr</option>
                            <option value="Professor"{{ (old('rep_title') == 'Professor') ? ' selected' : '' }}>
                                Professor
                            </option>
                            <option value="Other"{{ (old('rep_title') == 'Other') ? ' selected' : '' }}>Other</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Surname</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_surname" value="{{ old('rep_surname') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Given Names</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_given_names"
                               value="{{ old('rep_given_names') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Postal Address</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_address" value="{{ old('rep_address') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">State</label>
                    <div class="col-12 col-sm-10 ">
                        <select name="rep_state" class="custom-select">
                            <option value="10">Please Select</option>
                            @foreach($states as $state)
                                <option value="{{ $state->id }}"{{ (old('rep_state') == $state->id) ? ' selected' : '' }}>{{ $state->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Post Code</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_postcode" value="{{ old('rep_postcode') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2 col-form-label">Country</label>
                    <div class="col-12 col-sm-10 ">
                        <select name="rep_country" class="custom-select">
                            <option value="">Please Select</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}"{{ (old('rep_country') == $country->id) ? ' selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Telephone</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_telephone" value="{{ old('rep_telephone') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Mobile</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_mobile" value="{{ old('rep_mobile') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Email</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_email" value="{{ old('rep_email') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Facsimile</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control" name="rep_fax" value="{{ old('rep_fax') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-4 col-form-label">Relationship to you (e.g spouse, friend,
                        solicitor)</label>
                    <div class="col-12 col-sm-8">
                        <input type="text" class="form-control" name="rep_relation" value="{{ old('rep_relation') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12">Reasons for wanting to be represented</label>
                    <div class="col-12">
                        <textarea class="form-control" name="rep_reasons">{{ old('rep_reasons') }}</textarea>
                    </div>
                </div>
            </section>

            @if(old('c_type', $request->c_type)=='Disablement')
                <h5>Disability Claim and your Employment</h5>
                <section>
                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">What type of benefit is in dispute?</label>
                        <div class="col-12 col-sm-8">
                            <select name="dis_type" class="custom-select">
                                @foreach($disability_type as $type)
                                    <option value="{{ $type->id }}"{{ (old('dis_type') == $type->id) ? ' selected' : '' }}>{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">Date you were last physically at work?</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control datepicker" name="dis_last_date"
                                   value="{{ old('dis_last_date') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">Date you permanently ceased employment?</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control datepicker" name="dis_permanent_cease_date"
                                   value="{{ old('dis_permanent_cease_date') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">Reason for employment ceased (e.g. resignation, retrenchment)</label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="dis_reason" value="{{ old('dis_reason') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-md-8 col-lg-7 col-form-label">Did you permanently cease employment because of
                            the
                            physical or mental condition which caused you to make a disability claim?</label>
                        <div class="col-md-4 col-lg-5">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="dis_physical_mental"
                                           value="1"{{ (old('dis_physical_mental') == '1') ? ' checked' : '' }}>
                                    Yes
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="dis_physical_mental"
                                           value="2"{{ (old('dis_physical_mental') == '2') ? ' checked' : '' }}>
                                    No
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-md-8 col-lg-7 col-form-label">Did your Employer provide you with written
                            notification that your employment had ceased?</label>
                        <div class="col-md-4 col-lg-5">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="dis_employer_notice"
                                           value="1"{{ (old('dis_employer_notice') == '1') ? ' checked' : '' }}>
                                    Yes
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="dis_employer_notice"
                                           value="2"{{ (old('dis_employer_notice') == '2') ? ' checked' : '' }}>
                                    No
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">What date did you lodge your Disability claim with
                            the
                            Fund?</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control datepicker" name="dis_claim_lodge_date"
                                   value="{{ old('dis_claim_lodge_date') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-md-8 col-lg-7 col-form-label">Have you been in receipt of any workers'
                            compensation payments that relate to the physical or mental condition with which the
                            Disability
                            claim may relate?</label>
                        <div class="col-md-4 col-lg-5">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="dis_worker_compensation"
                                           value="1"{{ (old('dis_worker_compensation') == '1') ? ' checked' : '' }}>
                                    Yes
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="dis_worker_compensation"
                                           value="2"{{ (old('dis_worker_compensation') == '2') ? ' checked' : '' }}>
                                    No
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">If yes, what date did your payments
                            commence?</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control datepicker" name="dis_pay_start_date"
                                   value="{{ old('dis_pay_start_date') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">What date did the payments cease?</label>
                        <div class="col-12 col-sm-8">
                            <input type="text" class="form-control datepicker" name="dis_pay_cease_date"
                                   value="{{ old('dis_pay_cease_date') }}">
                        </div>
                    </div>
                </section>
            @endif

            <h5>Details of your Complaint</h5>
            <section>
                <div class="form-row">
                    <label class="col-12">Outline what has happened and state what particular decisions or actions of
                        the superannuation provider you are complaining about.
                        @if(old('c_type', $request->c_type)=='Other')
                            <br/>(Note: For complaints about reliance on information provided by or representations made
                            by a superannuation provider, please also provide the additional details in the nextsection)
                        @endif
                    </label>
                    <div class="col-12">
                        <textarea class="form-control"
                                  name="comp_about_details">{{ old('comp_about_details') }}</textarea>
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12">State why you consider the superannuation provider's decisions or actions are
                        unfair or unreasonable.</label>
                    <div class="col-12">
                        <textarea class="form-control" name="comp_unfair">{{ old('comp_unfair') }}</textarea>
                    </div>
                </div>
                @if(old('c_type', $request->c_type)!='Other')
                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">What is the amount of the benefit in
                            dispute?</label>

                        <div class="input-group mb-3 col-12 col-sm-8">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" name="comp_benefit"
                                   value="{{ old('comp_benefit') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">What outcomes are you seeking to resolve your complaint?</label>
                        <div class="col-12">
                            <textarea class="form-control" name="comp_outcomes">{{ old('comp_outcomes') }}</textarea>
                        </div>
                    </div>
                @endif
            </section>

            @if(old('c_type', $request->c_type)=='Other')
                <h5>Details of your complaint – reliance on information or representation</h5>
                <section>
                    <p>If your complaint is about reliance on information that the superannuation provider has made
                        available or provided to you, or about other representations that the superannuation provider
                        made
                        to you, please also provide the following further details.</p>

                    <div class="form-row">
                        <label class="col-12">What was the information or representation that you relied on?</label>
                        <div class="col-12">
                            <textarea class="form-control" name="rep_rely">{{ old('rep_rely') }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">What did the information or representation lead you to expect would
                            happen?</label>
                        <div class="col-12">
                            <textarea class="form-control" name="rep_lead">{{ old('rep_lead') }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">How did you rely on the information or representation?</label>
                        <div class="col-12">
                            <textarea class="form-control" name="rep_how">{{ old('rep_how') }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">What would you have done differently if you had known the correct or full
                            information or the representation had not been made?</label>
                        <div class="col-12">
                            <textarea class="form-control" name="rep_different">{{ old('rep_different') }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12 col-sm-4 col-form-label">What is the amount of the benefit in
                            dispute?</label>

                        <div class="input-group mb-3 col-12 col-sm-8">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" name="comp_benefit"
                                   value="{{ old('comp_benefit') }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">What outcomes are you seeking to resolve your complaint?</label>
                        <div class="col-12">
                            <textarea class="form-control" name="comp_outcomes">{{ old('comp_outcomes') }}</textarea>
                        </div>
                    </div>

                </section>
            @endif

            <h5>Declaration</h5>
            <section>
                <p>The above is a description of my complaint. I understand that copies of this information will be
                    given to my Fund or its representatives, or any other party to the complaint.</p>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Full name *</label>
                    <div class="col-12 col-sm-10 ">
                        <input type="text" class="form-control required" name="dec_fullname"
                               value="{{ old('dec_fullname') }}">
                    </div>
                </div>

                <div class="form-row">
                    <label class="col-12 col-sm-2">Email *</label>
                    <div class="col-12 col-sm-10">
                        <input type="text" class="form-control required email" name="dec_email"
                               value="{{ old('dec_email') }}">
                    </div>
                </div>

                <!-- <div class="form-row">
                    <label class="col-12 col-sm-2">Verify Yourself *</label>
                    <div class="col-12 col-sm-10">
                        <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                    </div>
                </div> -->
                @if(!$errors->isEmpty())
                    <div class="form-row">
                        <label class="col-12 col-sm-2"></label>
                        <div class="col-12 col-sm-10 val-error">
                            @if($errors->has('g-recaptcha-response'))
                                Please verify yourself
                            @else
                                Please fix errors
                            @endif
                        </div>
                    </div>
                @endif
            </section>

        </form>
    </div>

@endsection
@section('scripts')
    <script src="{{ asset('components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>
    <script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{ asset('js/site/complaint-form.js') }}"></script>
@endsection
@section('sec-inline-scripts')
<script type="text/javascript">
    @if(old('c_type'))
    var error_cont = true;
    @else
    var error_cont = false;
    @endif
</script>
@endsection
