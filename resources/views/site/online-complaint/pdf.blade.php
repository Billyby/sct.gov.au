<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
<style>
    body {font: 100%/1.1 Arial,Verdana,sans-serif;  font-size: 80%;}
    th{text-align: left;}
    td, th{padding-bottom: 15px;}
    h3 { text-decoration: underline; }
    .page-break {page-break-after: always;}
</style>
</head>
<body>
<table>
    <tr>
        <td style="width: 300px;"><img src="{{ asset('images/site/logo.png') }}" width="220" /></td>
        <td>
            <h1>
            @if($complaint->type->id==1)
                Registration of Complaint Form Death Benefit
            @endif
            @if($complaint->type->id==2)
                Registration of Complaint Form Disability Benefit
            @endif
            @if($complaint->type->id==3)
                Registration of Complaint Form General
            @endif
            </h1>
        </td>
    </tr>
</table>
<h3>Personal Details of the Complainant</h3>
<table>
    <tr>
        <th>Title:</th>
        <td>{{ $complaint->title }}</td>
    </tr>
    <tr>
        <th>Surname:</th>
        <td>{{ $complaint->surname }}</td>
    </tr>
    <tr>
        <th>Given Names:</th>
        <td>{{ $complaint->given_name }}</td>
    </tr>
    <tr>
        <th>Gender:</th>
        <td>{{ $complaint->gender }}</td>
    </tr>
    <tr>
        <th>Date of Birth:</th>
        <td>{{ $complaint->dob }}</td>
    </tr>
    <tr>
        <th>Postal Address:</th>
        <td>{{ $complaint->address }}</td>
    </tr>
    <tr>
        <th>State:</th>
        <td>{{ $complaint->state->name }}</td>
    </tr>
    <tr>
        <th>Post Code:</th>
        <td>{{ $complaint->postcode }}</td>
    </tr>
    <tr>
        <th>Country:</th>
        <td>
            @if(isset($complaint->country))
                {{ $complaint->country->name }}
            @endif
        </td>
    </tr>
    <tr>
        <th>Telephone:</th>
        <td>{{ $complaint->phone }}</td>
    </tr>
    <tr>
        <th>Mobile:</th>
        <td>{{ $complaint->mobile }}</td>
    </tr>
    <tr>
        <th>Email:</th>
        <td>{{ $complaint->email }}</td>
    </tr>
    <tr>
        <th>Fax:</th>
        <td>{{ $complaint->fax }}</td>
    </tr>
</table>
<table>
    <tr>
        <th colspan="2">If you are complaining on behalf of another person please provide their name and date of birth:</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->on_behalf_name_dob }}</td>
    </tr>
    <tr>
        <th colspan="2">Relationship to you:</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->relationship }}</td>
    </tr>
    <tr>
        <th colspan="2">If your complaint is on behalf of a child, are you the legal guardian?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->legal_guardian }}</td>
    </tr>
    <tr>
        <th colspan="2">If your complaint is on behalf of a person of legal age, do you hold a Power of Attorney?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->poa }}</td>
    </tr>
</table>

<div class="page-break"></div>

@if(isset($complaint->death))
    <h3>Personal Details of the Deceased Member</h3>
    <table class="table">
        <tr>
            <th>Surname:</th>
            <td>{{ $complaint->death->surname_deceased }}</td>
        </tr>
        <tr>
            <th>Given Names:</th>
            <td>{{ $complaint->death->given_name_deceased }}</td>
        </tr>
        <tr>
            <th>Date of Birth:</th>
            <td>{{ $complaint->death->date_deceased }}</td>
        </tr>
        <tr>
            <th>Your relationship to the deceased:</th>
            <td>{{ $complaint->death->relationship_deceased }}</td>
        </tr>
    </table>
@endif

<h3>Details of Superannuation Provider</h3>
<table>
    <tr>
        <th style="width:20%">Superannuation Provider Name:</th>
        <td>{{ $complaint->super_provider_name }}</td>
    </tr>
    <tr>
        <th>Member Name:</th>
        <td>{{ $complaint->super_member_name }}</td>
    </tr>
    <tr>
        <th>Member/Policy Number:</th>
        <td>{{ $complaint->super_member_policy_number }}</td>
    </tr>
</table>

<h3>Representation</h3>
<p>The Tribunal was established to be informal and economical and Complainants are expected to represent themselves. However, the Tribunal has the discretion under section23 of the Superannuation (Resolution of Complaints) Act 1993 to allow a party to a complaint to be represented by an agent if the Tribunal considers it to be necessary in all the circumstances.</p>
<p>If you wish to be represented by an agent, you need provide reasons as to why it is necessary that you be represented. Upon receipt of your reasons, the Tribunal will make a decision in relation to whether it allows or disallows such representation and advise you accordingly.</p>
<p>Please note that the Tribunal is not empowered to award costs in relation to any fees arising as a result of representation. In other words, YOU will have to bear the costs of your representation, if any.</p>

<table>
    <tr>
        <th>Title:</th>
        <td>{{ $complaint->rep_title }}</td>
    </tr>
    <tr>
        <th>Surname:</th>
        <td>{{ $complaint->rep_surname }}</td>
    </tr>
    <tr>
        <th>Given Names:</th>
        <td>{{ $complaint->rep_given_name }}</td>
    </tr>
    <tr>
        <th>Postal Address:</th>
        <td>{{ $complaint->rep_address }}</td>
    </tr>
    <tr>
        <th>State:</th>
        <td>{{ $complaint->repstate->name }}</td>
    </tr>
    <tr>
        <th>Post Code:</th>
        <td>{{ $complaint->rep_postcode }}</td>
    </tr>
    <tr>
        <th>Country:</th>
        <td>
            @if(isset($complaint->repcountry))
                {{ $complaint->repcountry->name }}
            @endif
        </td>
    </tr>
    <tr>
        <th>Telephone:</th>
        <td>{{ $complaint->rep_phone }}</td>
    </tr>
    <tr>
        <th>Mobile:</th>
        <td>{{ $complaint->rep_mobile }}</td>
    </tr>
    <tr>
        <th>Email:</th>
        <td>{{ $complaint->rep_email }}</td>
    </tr>
    <tr>
        <th>Facsimile:</th>
        <td>{{ $complaint->rep_fax }}</td>
    </tr>
    <tr>
        <th>Relationship to you:</th>
        <td>{{ $complaint->rep_relationship }}</td>
    </tr>
    <tr>
        <th colspan="2">Reasons for wanting to be represented:</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->rep_reasons }}</td>
    </tr>
</table>

@if(isset($complaint->disability))
<div class="page-break"></div>
<h3>Disability Claim and your Employment</h3>
<table>
    <tr>
        <th colspan="2">What type of benefit is in dispute?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->type->name }}</td>
    </tr>
    <tr>
        <th colspan="2">Date you were last physically at work?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_last_attended_date }}</td>
    </tr>
    <tr>
        <th colspan="2">Date you permanently ceased employment?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_ceased_work_date }}</td>
    </tr>
    <tr>
        <th colspan="2">Reason for employment ceased:</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_ceased_reasons }}</td>
    </tr>
    <tr>
        <th colspan="2">Did you permanently cease employment because of the physical or mental condition which caused you to make a disability claim?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_physical_mental }}</td>
    </tr>
    <tr>
        <th colspan="2">Did your Employer provide you with written notification that your employment had ceased?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_employer_notice }}</td>
    </tr>
    <tr>
        <th colspan="2">What date did you lodge your Disability claim with the Fund?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_claim_lodge_date }}</td>
    </tr>
    <tr>
        <th colspan="2">Have you been in receipt of any workers' compensation payments that relate to the physical or mental condition with which the Disability claim may relate?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_worker_compensation }}</td>
    </tr>
    <tr>
        <th colspan="2">If yes, what date did your payments commence?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_payment_start_date }}</td>
    </tr>
    <tr>
        <th colspan="2">What date did the payments cease?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->disability->dis_payment_ceased_date }}</td>
    </tr>
</table>
@endif

<div class="page-break"></div>

<h3>Details of your Complaint</h3>
<table>
    <tr>
        <th colspan="2">Outline what has happened and state what particular decisions or actions of the superannuation provider you are complaining about:</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->complaint_details }}</td>
    </tr>
    <tr>
        <th colspan="2">State why you consider the superannuation provider's decisions or actions are unfair or unreasonable:</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->complaint_unfair_details }}</td>
    </tr>
    @if($complaint->complaint_type_id!=3)
        <tr>
            <th colspan="2">What is the amount of the benefit in dispute?</th>
        </tr>
        <tr>
            <td colspan="2">
                @if(isset($complaint->complaint_amount_dispute))
                    ${{ $complaint->complaint_amount_dispute }}
                @endif
            </td>
        </tr>
        <tr>
            <th colspan="2">What outcomes are you seeking to resolve your complaint?</th>
        </tr>
        <tr>
            <td colspan="2">{{ $complaint->complaint_outcomes }}</td>
        </tr>
    @endif
</table>

@if(isset($complaint->other))
<h3>Details of your complaint – reliance on information or representation</h3>
<table>
    <tr>
        <th colspan="2">What was the information or representation that you relied on?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->other->all_info_rep_relied }}</td>
    </tr>
    <tr>
        <th colspan="2">What did the information or representation lead you to expect would happen?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->other->all_info_rep_expectation }}</td>
    </tr>
    <tr>
        <th colspan="2">How did you rely on the information or representation?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->other->all_info_rep_how }}</td>
    </tr>
    <tr>
        <th colspan="2">What would you have done differently if you had known the correct or full information or the representation had not been made?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->other->all_info_rep_action }}</td>
    </tr>
    <tr>
        <th colspan="2">What is the amount of the benefit in dispute?</th>
    </tr>
    <tr>
        <td colspan="2">
            @if(isset($complaint->complaint_amount_dispute))
                ${{ $complaint->complaint_amount_dispute }}
            @endif
        </td>
    </tr>
    <tr>
        <th colspan="2">What outcomes are you seeking to resolve your complaint?</th>
    </tr>
    <tr>
        <td colspan="2">{{ $complaint->complaint_outcomes }}</td>
    </tr>
</table>
@endif

<h3>Declaration</h3>
<p>The above is a description of my complaint. I understand that copies of this information, and any documents I have enclosed, or provide to the Tribunal at a later stage, will be given to my superannuation provider or its representatives, and any other party to the complaint.</p>
<table>
    <tr>
        <th>Print Name:</th>
        <td>{{ $complaint->complainant_name }}</td>
    </tr>
    <tr>
        <th>Complaint Submitted Date:</th>
        <td>{{ $complaint->created_at->format('d  M  Y h:i:s A') }}</td>
    </tr>
</table>

<div class="page-break"></div>

<h3>Personal Information Collection Notice</h3>
<h4>Collection of personal information</h4>
<p>We collect your personal information for the purposes of performing our statutory functions under the <i>Superannuation (Resolution of Complaints) Act 1993</i> (Complaints Act).  These functions relate to the resolution of complaints made to us about decisions made by superannuation entities.</p>
<p>We collect personal information about you through making a complaint to us and you participation in our complaints handling processes.  We also collect personal information about you from other parties to your complaint.  These other parties usually include:</p>

<ul>
    <li>the trustees of the superannuation entities complained about</li>
    <li>any relevant insurer</li>
    <li>in relation to a complaint about the distribution of a death benefit, other persons who may claim an entitlement to all or part of the benefit.</li>
</ul>

<p>If this information is not collected by the Tribunal it may not be able to properly investigate and resolve your complaint.  The Complaints Act may require or authorise the Tribunal to collect this information.</p>

<h4>Disclosure of your personal information</h4>
<p>We disclose personal information for the purposes of our statutory functions under the Complaints Act.  This includes disclosures to other parties to a complaint to ensure that we accord procedural fairness to all parties in our dispute resolution processes.</p>
<p>We may also disclose your personal information to other persons where you consent or there are other circumstances where disclosure is permitted under the <i>Privacy Act 1988</i>.  These circumstances include disclosure to appropriate persons where:</p>

<ul>
    <li>the disclosure is required or authorised by law</li>
    <li>you would reasonably expect that the information is used for that purpose and the purpose relates to the our functions</li>
    <li>it will lessen or prevent a serious threat to somebody's life, health or safety, or to public health or safety</li>
    <li>it is reasonably necessary for enforcement related activities of an enforcement body.</li>
</ul>

<p>In dealing with a complaint it is possible that we may be required to disclose personal information to a person involved in the complaint who is outside Australia.  Disclosure of personal information to an overseas recipient depends on the facts of the particular complaint, and this could occur in relation to any foreign country.</p>

<h4>Privacy policy</h4>
<p>Our Privacy Policy contains information about:</p>
<ul>
    <li>how you can seek access to personal information that we hold about you or request that we make corrections to that information</li>
    <li>how you may make a complaint about our handling of your personal information. </li>
</ul>
<p>You can read our complete Privacy Policy on our website at www.sct.gov.au or you can request that we provide you with a copy by contacting us on the Contact Details on our website.</p>


</body>
</html>
