@extends('site.layouts.app')

@section('content')

    @include('site/partials/carousel-inner')

        <div class="container">

            <h2>Personal Information Collection Notice</h2>
            <p class="heading">Collection of personal information</p>
            <p>We collect your personal information for the purposes of performing our statutory functions under the <i>Superannuation (Resolution of Complaints) Act 1993</i> (Complaints Act).  These functions relate to the resolution of complaints made to us about decisions made by superannuation entities.</p>
            <p>We collect personal information about you through making a complaint to us and you participation in our complaints handling processes.  We also collect personal information about you from other parties to your complaint.  These other parties usually include:</p>

            <ul>
                <li>the trustees of the superannuation entities complained about</li>
                <li>any relevant insurer</li>
                <li>in relation to a complaint about the distribution of a death benefit, other persons who may claim an entitlement to all or part of the benefit.</li>
            </ul>

            <p>If this information is not collected by the Tribunal it may not be able to properly investigate and resolve your complaint.  The Complaints Act may require or authorise the Tribunal to collect this information.</p>

            <p class="heading">Disclosure of your personal information</p>
            <p>We disclose personal information for the purposes of our statutory functions under the Complaints Act.  This includes disclosures to other parties to a complaint to ensure that we accord procedural fairness to all parties in our dispute resolution processes.</p>
            <p>We may also disclose your personal information to other persons where you consent or there are other circumstances where disclosure is permitted under the <i>Privacy Act 1988</i>.  These circumstances include disclosure to appropriate persons where:</p>

            <ul>
                <li>the disclosure is required or authorised by law</li>
                <li>you would reasonably expect that the information is used for that purpose and the purpose relates to the our functions</li>
                <li>it will lessen or prevent a serious threat to somebody's life, health or safety, or to public health or safety</li>
                <li>it is reasonably necessary for enforcement related activities of an enforcement body.</li>
            </ul>

            <p>In dealing with a complaint it is possible that we may be required to disclose personal information to a person involved in the complaint who is outside Australia.  Disclosure of personal information to an overseas recipient depends on the facts of the particular complaint, and this could occur in relation to any foreign country.</p>

            <p class="heading">Privacy policy</p>
            <p>Our Privacy Policy contains information about:</p>
            <ul>
                <li>how you can seek access to personal information that we hold about you or request that we make corrections to that information</li>
                <li>how you may make a complaint about our handling of your personal information. </li>
            </ul>
            <p>You can read our complete Privacy Policy on our website at www.sct.gov.au or you can request that we provide you with a copy by contacting us on the Contact Details on our website.</p>

            <form method="post" action="{{ url('online-complaint/registration-form') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="fc_officer" value="{{ $request->fc_officer }}">
                <input type="hidden" name="fc_response" value="{{ $request->fc_response }}">
                <input type="hidden" name="expiry" value="{{ $request->expiry }}">
                <input type="hidden" name="c_type" value="{{ $request->c_type }}">
                <button id="submit" type="submit" class="btn btn-secondary btn-sm btn-block">Commence Registration of Complaint Form »</button>
            </form>
        </div><!-- /.container -->

@endsection