@extends('site/layouts/app')


@section('styles')
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/iCheck/all.css') }}"
          xmlns="http://www.w3.org/1999/html">
@endsection
@section('content')

    @include('site/partials/carousel-inner')

        <div class="container">

            <h2>Online Complaint Registration Form</h2>

            <form method="post" action="{{ url('online-complaint/personal-information-collection-notice') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <fieldset class="form-group row">
                    <legend class="col-form-legend col-sm-12">Have you made a complaint to your Fund's Complaints Officer?</legend>
                    <div class="col-sm-12">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="fc_officer" id="fc-officer-yes" value="Yes">
                                Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="fc_officer" id="fc-officer-no" value="No">
                                No
                            </label>
                        </div>
                        <div class="sub-note invisible" id="fc-officer-note">You MUST make a formal, written complaint to your Fund's Complaints Officer before the Tribunal can assist you. Note that TIME LIMITS APPLY to certain complaints. Contact the Tribunal on 1300 884 114 for details. Click here to view a sample complaint letter.</div>
                    </div>
                </fieldset>

                <fieldset id="funds-response" class="form-group row invisible">
                    <legend class="col-form-legend col-sm-12">Has the Fund responded to your complaint?</legend>
                    <div class="col-sm-12">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="fc_response" id="fc-response-yes" value="Yes">
                                Yes, the fund responded to my complaint but I am not satisfied with the response.
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="fc_response" id="fc-response-no" value="No">
                                No
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset id="funds-response-no" class="form-group row invisible">
                    <legend class="col-form-legend col-sm-12">Has 90 days passed since the Fund received your complaint?</legend>
                    <div class="col-sm-12">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="expiry" id="expiry-yes" value="Yes">
                                Yes, the fund responded to my complaint but I am not satisfied with the response.
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="expiry" id="expiry-no" value="No">
                                No
                            </label>
                        </div>
                        <div class="sub-note invisible" id="funds-response-no-note">You MUST make a formal, written complaint to your Fund's Complaints Officer before the Tribunal can assist you. Note that TIME LIMITS APPLY to certain complaints. Contact the Tribunal on 1300 884 114 for details. Click here to view a sample complaint letter.</div>
                    </div>
                </fieldset>

                <fieldset id="complaint-type" class="form-group row invisible">
                    <legend class="col-form-legend col-sm-12">Please select the type of complaint you need to make.</legend>
                    <div class="col-sm-12">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="c_type" id="c-type-death" value="Death">
                                Death
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="c_type" id="c-type-disability" value="Disablement">
                                Disablement
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="c_type" id="c-type-other" value="Other">
                                Other
                            </label>
                        </div>
                    </div>
                </fieldset>
                <button id="submit" type="submit" class="btn btn-secondary btn-sm btn-block invisible">Read the Personal Information Collection Notice and commence Registration of Complaint Form »</button>

            </form>

            <div><strong>Note:</strong> Any information sent to the Tribunal using this online complaint form is encrypted using Secured Socket Layer (SSL). Refer to our website’s privacy policy for more information.</div>

        </div><!-- /.container -->

@endsection
@section('scripts')
    <script src="{{ asset('components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/site/online-complaint.js') }}"></script>
@endsection
