@extends('site.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload-ui.css') }}">
@endsection
@section('content')

    @include('site/partials/carousel-inner')

    <div class="container online-complaint">

        <form id="fileupload" enctype="multipart/form-data" method="POST" action="{{ url('online-complaint/success') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $complaint_id }}">
            <input type="hidden" name="receipt_number" value="{{ $receipt_number }}">
            <input type="hidden" id="sub_cont" name="sub_cont" value="0">

            <h5>Upload Files</h5>
                <p>Please follow the instructions below to upload supporting documentation for your complaint
                    registration.</p>

                <ol>
                    <li>To attach a file select "Add files..." and choose one or more files to upload from your
                        computer. You may upload a maximum of 10 documents
                    </li>
                    <li>Select "Upload All" to upload all listed files, or "Upload File" to upload files
                        individually<br><em>When the upload is complete, for one or more files, the blue progress bar
                            should no longer be visible.</em><br></li>
                    <li>If you wish to stop the uploading process select either "Cancel All" or "Cancel File" as
                        appropriate
                    </li>
                    <li>To delete multiple files select the check boxes as appropriate and select "Delete All"; to
                        remove a single file select the "Delete File" button next to the file
                    </li>
                    <li>Once finished uploading documents <strong><u>you must select "Submit Complaint Form" to send
                                your complaint form</u></strong> to the SCT for review<br><em>Note: Once submitted you
                            will no longer be able to upload documents</em></li>
                </ol>

                <div class="row fileupload-buttonbar">
                    <div class="col-lg-12">
                        <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                    </span>
                        <button type="submit" class="btn btn-primary start">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <button type="reset" class="btn btn-warning cancel">
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            <span>Cancel upload</span>
                        </button>
                        <button type="button" class="btn btn-danger delete">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" class="toggle">
                        <span class="fileupload-process"></span>
                    </div>
                    <div class="col-lg-5 fileupload-progress fade">
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                             aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
                <table role="presentation" class="table table-striped">
                    <tbody class="files"></tbody>
                </table>
            <button id="submit-btn" type="button" class="btn btn-secondary btn-sm btn-block">Submit Complaint Form</button>
        </form>
    </div>

@endsection
@section('scripts')
    <script src="{{ asset('components/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ asset('components/blueimp-tmpl/js/tmpl.min.js') }}"></script>
    <script src="{{ asset('components/blueimp-load-image/js/load-image.all.min.js') }}"></script>
    <script src="{{ asset('components/blueimp-canvas-to-blob/js/canvas-to-blob.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload-ui.js') }}"></script>
    <script src="{{ asset('js/site/upload.js') }}"></script>
@endsection
@section('sec-inline-scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload">
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
           <input type="text" class="form-control form-control-sm" name="title[]" placeholder="Title" style="margin-bottom:5px;">
           <input type="text" class="form-control form-control-sm" name="description[]" placeholder="Description">
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td style="text-align: right">
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel c-btn">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download">
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
           <strong>{%=file.title%}</strong><br/>{%=file.description%}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td style="text-align: right">
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
@endsection
