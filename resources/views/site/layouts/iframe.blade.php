@if(isset($mode) && $mode == 'preview')
{{$meta_title_inner = "Preview Page"}}
@endif
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="Contact">
    <meta name="description" content="Contact">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">  
    <meta name="robots" content="all">
    <title>Contact</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">

    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/contact.css') }}">
   
    @yield('styles')

</head>
<body>
@yield('content')   

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
@yield('scripts')
@yield('inline-scripts')
</body>
</html>
