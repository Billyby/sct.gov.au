<!DOCTYPE html>
<html>
<body>
<head>
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
    <style>
        body {
            font: 100%/1.4 'Dosis', Arial,Verdana,sans-serif;
        }
    </style>
</head>
<img src="https://webdev.sct.gov.au/images/site/email-logo-header.gif">
<div style="margin-bottom: 20px;">
    {!! $mail_content !!}
</div>
<img src="https://webdev.sct.gov.au/images/site/email-logo-footer.gif">
</body>
</html>
