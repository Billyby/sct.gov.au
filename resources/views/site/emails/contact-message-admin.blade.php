<!DOCTYPE html>
<html>
<body>
<head>
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
    <style>
        body {
            font: 100%/1.4 'Dosis', Arial,Verdana,sans-serif;
        }
    </style>
</head>
<img src="https://webdev.sct.gov.au/images/site/email-logo-header.gif">
<h3>New Contact Message</h3>
<table class="table" style="margin-top: 15px; margin-bottom: 15px;">
    @foreach(json_decode($contact_message->data) as $field)
        <tr>
            <th style="width:20%; text-align: left">{{ ucwords($field->field) }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
</table>
<img src="https://webdev.sct.gov.au/images/site/email-logo-footer.gif">
</body>
</html>
