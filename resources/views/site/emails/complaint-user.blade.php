<!DOCTYPE html>
<html>
<body>
<h2>Hi {{ $complaint->given_name.' '.$complaint->surname }}</h2>
This is an auto generated response from the Superannuation Complaints Tribunal's (SCT) Online Complaint registration form.<br />
This email confirms that your online complaint registration form is saved as a draft with the SCT.<br /><br />
Receipt Number: <strong>{{ $complaint->complaint_receipt_number }}</strong><br /><br />
<a href="{{ url('online-complaint/'.$complaint->complaint_receipt_number.'/pdf') }}" target="_blank">Click here</a> to download your Online Complaint Form in PDF Format.<br /><br />
Using the receipt number and your email address, you can upload documents to support your complaint registration form.<br /><br />
<i>Note: Once you have submitted your application to SCT for review, you can no longer be able to upload more documents using this information.</i><br /><br />
Regards,<br />
Superannuation Complaints Tribunal<br />
1300 884 114
</body>
</html>
