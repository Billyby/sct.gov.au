<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <style>
        body {
            font: 100%/1.4 'Dosis', Arial,Verdana,sans-serif;
        }
    </style>
</head>
<body>
<h4> <i class="fas fa-check"></i> Thank you for your enquiry. A Superannuation Complaints Tribunal representative will contact you shortly.</h4>
</body>
</html>
