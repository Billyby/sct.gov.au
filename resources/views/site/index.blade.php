@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')


<!-- Intro Text -->
<div class="row featurette">
	<div id="introWrapper">
		<div id="intro">
		   <div class="introPanelOne"><img alt="" src="{{ url('') }}/images/site/pic1.jpg" title="What we do" alt="What we do" /></div>

		   <div class="introPanelTwo">
		      <div class="introPanelTwo-txt">
		         {!! $home_intro_text !!}
		         
				  <!-- <p><a role="button" href="{{ url('pages/about-us/what-we-do') }}" class="btn-submit" >Learn More</a></p> -->
		      </div>
		   </div>
	    </div>
   </div>
</div>

<div class='footerSearch'> 
   @include('site/partials/search')
</div>
@endsection

